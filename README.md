# [AES Inspector](mailto:tiju.oliver@gmail.com) #

AES Inspector is an Android app that allows users to view the hidden state of an AES cipher.

### How do I get set up? ###

1. Download & install the latest version of [Android Studio](https://developer.android.com/studio).
2. Open the existing project in Android Studio
3. Insert your AdMob IDs and privacy link in 'app/src/res/values/strings.xml' by uncommenting the appropriate lines.
4. Edit key.properties before release