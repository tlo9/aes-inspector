package me.tiju.aes;

import android.content.Context;
import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class RijndaelInstrumentationTest
{
    private Context context;

    @Before
    public void getContext()
    {
        context = InstrumentationRegistry.getTargetContext();
    }

    @Test
    public void testExpandKey()
    {
        int i=0;
        int[] key1 = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        int[] key2 = new int[] { 0x69, 0x20, 0xe2, 0x99, 0xa5, 0x20, 0x2a, 0x6d, 0x65, 0x6e,
                0x63, 0x68, 0x69, 0x74, 0x6f, 0x2a };
        int[] key3 = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0 };
        int[] key4 = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ,13, 14, 15, 16, 17,
                18, 19, 20, 21, 22, 23 };
        int[] correctExpandedKey1 = new int[] {
                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                0x62, 0x63, 0x63, 0x63, 0x62, 0x63, 0x63, 0x63, 0x62, 0x63, 0x63, 0x63, 0x62, 0x63, 0x63, 0x63,
                0x9b, 0x98, 0x98, 0xc9, 0xf9, 0xfb, 0xfb, 0xaa, 0x9b, 0x98, 0x98, 0xc9, 0xf9, 0xfb, 0xfb, 0xaa,
                0x90, 0x97, 0x34, 0x50, 0x69, 0x6c, 0xcf, 0xfa, 0xf2, 0xf4, 0x57, 0x33, 0x0b, 0x0f, 0xac, 0x99,
                0xee, 0x06, 0xda, 0x7b, 0x87, 0x6a, 0x15, 0x81, 0x75, 0x9e, 0x42, 0xb2, 0x7e, 0x91, 0xee, 0x2b,
                0x7f, 0x2e, 0x2b, 0x88, 0xf8, 0x44, 0x3e, 0x09, 0x8d, 0xda, 0x7c, 0xbb, 0xf3, 0x4b, 0x92, 0x90,
                0xec, 0x61, 0x4b, 0x85, 0x14, 0x25, 0x75, 0x8c, 0x99, 0xff, 0x09, 0x37, 0x6a, 0xb4, 0x9b, 0xa7,
                0x21, 0x75, 0x17, 0x87, 0x35, 0x50, 0x62, 0x0b, 0xac, 0xaf, 0x6b, 0x3c, 0xc6, 0x1b, 0xf0, 0x9b,
                0x0e, 0xf9, 0x03, 0x33, 0x3b, 0xa9, 0x61, 0x38, 0x97, 0x06, 0x0a, 0x04, 0x51, 0x1d, 0xfa, 0x9f,
                0xb1, 0xd4, 0xd8, 0xe2, 0x8a, 0x7d, 0xb9, 0xda, 0x1d, 0x7b, 0xb3, 0xde, 0x4c, 0x66, 0x49, 0x41,
                0xb4, 0xef, 0x5b, 0xcb, 0x3e, 0x92, 0xe2, 0x11, 0x23, 0xe9, 0x51, 0xcf, 0x6f, 0x8f, 0x18, 0x8e };
        int[] correctExpandedKey2 = new int[] {
                0x69, 0x20, 0xe2, 0x99, 0xa5, 0x20, 0x2a, 0x6d, 0x65, 0x6e, 0x63, 0x68, 0x69, 0x74, 0x6f, 0x2a,
                0xfa, 0x88, 0x07, 0x60, 0x5f, 0xa8, 0x2d, 0x0d, 0x3a, 0xc6, 0x4e, 0x65, 0x53, 0xb2, 0x21, 0x4f,
                0xcf, 0x75, 0x83, 0x8d, 0x90, 0xdd, 0xae, 0x80, 0xaa, 0x1b, 0xe0, 0xe5, 0xf9, 0xa9, 0xc1, 0xaa,
                0x18, 0x0d, 0x2f, 0x14, 0x88, 0xd0, 0x81, 0x94, 0x22, 0xcb, 0x61, 0x71, 0xdb, 0x62, 0xa0, 0xdb,
                0xba, 0xed, 0x96, 0xad, 0x32, 0x3d, 0x17, 0x39, 0x10, 0xf6, 0x76, 0x48, 0xcb, 0x94, 0xd6, 0x93,
                0x88, 0x1b, 0x4a, 0xb2, 0xba, 0x26, 0x5d, 0x8b, 0xaa, 0xd0, 0x2b, 0xc3, 0x61, 0x44, 0xfd, 0x50,
                0xb3, 0x4f, 0x19, 0x5d, 0x09, 0x69, 0x44, 0xd6, 0xa3, 0xb9, 0x6f, 0x15, 0xc2, 0xfd, 0x92, 0x45,
                0xa7, 0x00, 0x77, 0x78, 0xae, 0x69, 0x33, 0xae, 0x0d, 0xd0, 0x5c, 0xbb, 0xcf, 0x2d, 0xce, 0xfe,
                0xff, 0x8b, 0xcc, 0xf2, 0x51, 0xe2, 0xff, 0x5c, 0x5c, 0x32, 0xa3, 0xe7, 0x93, 0x1f, 0x6d, 0x19,
                0x24, 0xb7, 0x18, 0x2e, 0x75, 0x55, 0xe7, 0x72, 0x29, 0x67, 0x44, 0x95, 0xba, 0x78, 0x29, 0x8c,
                0xae, 0x12, 0x7c, 0xda, 0xdb, 0x47, 0x9b, 0xa8, 0xf2, 0x20, 0xdf, 0x3d, 0x48, 0x58, 0xf6, 0xb1 };
        String correctExpandedKey3Str = "00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 62 63 63 63 62 63 63 63 62 63 63 63 62 63 63 63 62 63 63 63 62 63 63 63 9b 98 98 c9 f9 fb fb aa 9b 98 98 c9 f9 fb fb aa 9b 98 98 c9 f9 fb fb aa 90 97 34 50 69 6c cf fa f2 f4 57 33 0b 0f ac 99 90 97 34 50 69 6c cf fa c8 1d 19 a9 a1 71 d6 53 53 85 81 60 58 8a 2d f9 c8 1d 19 a9 a1 71 d6 53 7b eb f4 9b da 9a 22 c8 89 1f a3 a8 d1 95 8e 51 19 88 97 f8 b8 f9 41 ab c2 68 96 f7 18 f2 b4 3f 91 ed 17 97 40 78 99 c6 59 f0 0e 3e e1 09 4f 95 83 ec bc 0f 9b 1e 08 30 0a f3 1f a7 4a 8b 86 61 13 7b 88 5f f2 72 c7 ca 43 2a c8 86 d8 34 c0 b6 d2 c7 df 11 98 4c 59 70";
        String correctExpandedKey4Str = "00 01 02 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f "
                                        + "10 11 12 13 14 15 16 17 58 46 f2 f9 5c 43 f4 fe "
                                        + "54 4a fe f5 58 47 f0 fa 48 56 e2 e9 5c 43 f4 fe "
                                        + "40 f9 49 b3 1c ba bd 4d 48 f0 43 b8 10 b7 b3 42 "
                                        + "58 e1 51 ab 04 a2 a5 55 7e ff b5 41 62 45 08 0c "
                                        + "2a b5 4b b4 3a 02 f8 f6 62 e3 a9 5d 66 41 0c 08 "
                                        + "f5 01 85 72 97 44 8d 7e bd f1 c6 ca 87 f3 3e 3c "
                                        + "e5 10 97 61 83 51 9b 69 34 15 7c 9e a3 51 f1 e0 "
                                        + "1e a0 37 2a 99 53 09 16 7c 43 9e 77 ff 12 05 1e "
                                        + "dd 7e 0e 88 7e 2f ff 68 60 8f c8 42 f9 dc c1 54 "
                                        + "85 9f 5f 23 7a 8d 5a 3d c0 c0 29 52 be ef d6 3a "
                                        + "de 60 1e 78 27 bc df 2c a2 23 80 0f d8 ae da 32 "
                                        + "a4 97 0a 33 1a 78 dc 09 c4 18 c2 71 e3 a4 1d 5d";
        int[] correctExpandedKey3 = new int[208];
        int[] correctExpandedKey4 = new int[208];

        int[] correctExpandedKey5 = new int[240];
        int[] correctExpandedKey6 = new int[240];

        String key5Str = "00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00";
        String key6Str = "ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff";
        String correctExpandedKey5Str = "00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 "
                                + "00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 "
                                + "62 63 63 63 62 63 63 63 62 63 63 63 62 63 63 63 "
                                + "aa fb fb fb aa fb fb fb aa fb fb fb aa fb fb fb "
                                + "6f 6c 6c cf 0d 0f 0f ac 6f 6c 6c cf 0d 0f 0f ac "
                                + "7d 8d 8d 6a d7 76 76 91 7d 8d 8d 6a d7 76 76 91 "
                                + "53 54 ed c1 5e 5b e2 6d 31 37 8e a2 3c 38 81 0e "
                                + "96 8a 81 c1 41 fc f7 50 3c 71 7a 3a eb 07 0c ab "
                                + "9e aa 8f 28 c0 f1 6d 45 f1 c6 e3 e7 cd fe 62 e9 "
                                + "2b 31 2b df 6a cd dc 8f 56 bc a6 b5 bd bb aa 1e "
                                + "64 06 fd 52 a4 f7 90 17 55 31 73 f0 98 cf 11 19 "
                                + "6d bb a9 0b 07 76 75 84 51 ca d3 31 ec 71 79 2f "
                                + "e7 b0 e8 9c 43 47 78 8b 16 76 0b 7b 8e b9 1a 62 "
                                + "74 ed 0b a1 73 9b 7e 25 22 51 ad 14 ce 20 d4 3b "
                                + "10 f8 0a 17 53 bf 72 9c 45 c9 79 e7 cb 70 63 85 ";
        String correctExpandedKey6Str = "ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff "
                                        + "ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff "
                                        + "e8 e9 e9 e9 17 16 16 16 e8 e9 e9 e9 17 16 16 16 "
                                        + "0f b8 b8 b8 f0 47 47 47 0f b8 b8 b8 f0 47 47 47 "
                                        + "4a 49 49 65 5d 5f 5f 73 b5 b6 b6 9a a2 a0 a0 8c "
                                        + "35 58 58 dc c5 1f 1f 9b ca a7 a7 23 3a e0 e0 64 "
                                        + "af a8 0a e5 f2 f7 55 96 47 41 e3 0c e5 e1 43 80 "
                                        + "ec a0 42 11 29 bf 5d 8a e3 18 fa a9 d9 f8 1a cd "
                                        + "e6 0a b7 d0 14 fd e2 46 53 bc 01 4a b6 5d 42 ca "
                                        + "a2 ec 6e 65 8b 53 33 ef 68 4b c9 46 b1 b3 d3 8b "
                                        + "9b 6c 8a 18 8f 91 68 5e dc 2d 69 14 6a 70 2b de "
                                        + "a0 bd 9f 78 2b ee ac 97 43 a5 65 d1 f2 16 b6 5a "
                                        + "fc 22 34 91 73 b3 5c cf af 9e 35 db c5 ee 1e 05 "
                                        + "06 95 ed 13 2d 7b 41 84 6e de 24 55 9c c8 92 0f "
                                        + "54 6d 42 4f 27 de 1e 80 88 40 2b 5b 4d ae 35 5e";
        int[] key5 = new int[32], key6 = new int[32];

        for( String s : correctExpandedKey3Str.split(" ") )
            correctExpandedKey3[i++] = Integer.parseInt(s, 16);

        i = 0;

        for( String s : correctExpandedKey4Str.split(" ") )
            correctExpandedKey4[i++] = Integer.parseInt(s, 16);

        i = 0;

        for( String s : correctExpandedKey5Str.split(" ") )
            correctExpandedKey5[i++] = Integer.parseInt(s, 16);

        i = 0;

        for( String s : correctExpandedKey6Str.split(" ") )
            correctExpandedKey6[i++] = Integer.parseInt(s, 16);

        i = 0;

        for( String s : key5Str.split(" ") )
            key5[i++] = Integer.parseInt(s, 16);

        i = 0;

        for( String s : key6Str.split(" ") )
            key6[i++] = Integer.parseInt(s, 16);

        int[] expandedKey, expandedKey2;

        expandedKey = Rijndael.expandKey(context, null, key1, 128);
        assertArrayEquals("Incorrect expanded key.", correctExpandedKey1, expandedKey);
        expandedKey2 = Rijndael.expandKey(context, expandedKey, key2, 128);
        assertArrayEquals("Incorrect expanded key.", correctExpandedKey2, expandedKey);
        assertEquals(expandedKey, expandedKey2);

        expandedKey = Rijndael.expandKey(context, null, key3, 192);
        assertArrayEquals("Incorrect expanded key.", correctExpandedKey3, expandedKey);
        expandedKey2 = Rijndael.expandKey(context, expandedKey, key4, 192);
        assertArrayEquals("Incorrect expanded key.", correctExpandedKey4, expandedKey);
        assertEquals(expandedKey, expandedKey2);

        expandedKey = Rijndael.expandKey(context, null, key5, 256);
        assertArrayEquals("Incorrect expanded key.", correctExpandedKey5, expandedKey);
        expandedKey2 = Rijndael.expandKey(context, expandedKey, key6, 256);
        assertArrayEquals("Incorrect expanded key.", correctExpandedKey6, expandedKey);
        assertEquals(expandedKey, expandedKey2);
    }

    @Test
    public void testEncrypt()
    {
        final int[] key = { 0x69, 0x20, 0xe2, 0x99, 0xa5, 0x20, 0x2a, 0x6d, 0x65, 0x6e,
                0x63, 0x68, 0x69, 0x74, 0x6f, 0x2a };
        final int[] key2 = {105, 32, 226, 153, 165, 32, 42, 109, 101, 110, 99, 104, 105, 116, 111,
                42, 97, 98, 99, 100, 101, 102, 103, 104};
        final int[] key3 = {105, 32, 226, 153, 165, 32, 42, 109, 101, 110, 99, 104, 105, 116, 111,
                42, 97, 98, 99, 100, 101, 102, 103, 104, 65, 66, 67, 68, 69, 70, 71, 72};
        final int[] plaintext = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
        int[] ciphertext;
        final int[] correctCiphertext = {0, 124, 218, 125, 49, 113, 222, 163, 125, 79, 32, 78, 196, 254, 214, 30};
        final int[] correctCiphertext2 = {249, 11, 134, 24, 186, 127, 227, 194, 196, 7, 51, 71, 8, 64, 26, 8};
        final int[] correctCiphertext3 = {204, 15, 213, 65, 22, 156, 30, 56, 27, 244, 22, 250, 68, 33, 234, 98};

        ciphertext = Rijndael.encrypt(context, key, plaintext, null);
        assertArrayEquals("Incorrect ciphertext.", correctCiphertext, ciphertext);
        ciphertext = Rijndael.encrypt(context, key2, plaintext, null);
        assertArrayEquals("Incorrect ciphertext.", correctCiphertext2, ciphertext);
        ciphertext = Rijndael.encrypt(context, key3, plaintext, null);
        assertArrayEquals("Incorrect ciphertext.", correctCiphertext3, ciphertext);
    }
}
