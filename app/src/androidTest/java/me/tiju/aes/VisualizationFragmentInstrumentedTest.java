package me.tiju.aes;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import androidx.test.InstrumentationRegistry;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.action.ViewActions;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import static androidx.test.espresso.Espresso.*;
import static androidx.test.espresso.action.ViewActions.*;
import static androidx.test.espresso.assertion.ViewAssertions.*;
import static androidx.test.espresso.matcher.ViewMatchers.*;
import static org.hamcrest.core.IsNot.not;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class VisualizationFragmentInstrumentedTest
{
    @Rule
    public ActivityTestRule<VisualizationActivity> mActivityRule =
            new ActivityTestRule<>(VisualizationActivity.class);

    private void rotateScreen()
    {
        Context context = InstrumentationRegistry.getTargetContext();
        int orientation
                = context.getResources().getConfiguration().orientation;

        Activity activity = mActivityRule.getActivity();
        activity.setRequestedOrientation(
                (orientation == Configuration.ORIENTATION_PORTRAIT) ?
                        ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE :
                        ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Test
    public void testCollapseActionMenu()
    {
        Context context = InstrumentationRegistry.getTargetContext();
        openActionBarOverflowOrOptionsMenu(context);

        onView(withText(context.getString(R.string.menu_settings))).check(matches(isEnabled()));
        onView(withText(context.getString(R.string.menu_sbox))).check(matches(isEnabled()));
        onView(withText(context.getString(R.string.menu_about))).check(matches(isEnabled()));
    }

    @Test
    public void testGridAndCiphertextHidden()
    {
        onView(withId(R.id.gridScrollView)).check(matches(not(isDisplayed())));
        onView(withId(R.id.ciphertextLayout)).check(matches(not(isDisplayed())));
    }

    @Test
    public void testEncryptButtonShown()
    {
        onView(withId(R.id.menu_encrypt)).check(matches(isClickable()));
        onView(withId(R.id.menu_submit)).check(doesNotExist());
    }

    @Test
    public void testPlaintextFragmentViews()
    {
        // Make sure we only see the submit button in action bar, not the encrypt button

        onView(withId(R.id.plaintextView)).perform(click());
        onView(withId(R.id.menu_encrypt)).check(doesNotExist());
        onView(withId(R.id.menu_submit)).check(matches(isClickable()));

        // Are the radio buttons visible?

        onView(withId(R.id.textRadioButton)).check(matches(isDisplayed()));
        onView(withId(R.id.hexRadioButton)).check(matches(isDisplayed()));
        onView(withId(R.id.fileRadioButton)).check(matches(isDisplayed()));

        onView(withId(R.id.textPlaintextData)).check(matches(not(isDisplayed())));
        onView(withId(R.id.hexPlaintextData)).check(matches(not(isDisplayed())));
        onView(withId(R.id.fileOffset)).check(matches(not(isDisplayed())));
        onView(withId(R.id.filename)).check(matches(not(isDisplayed())));
        onView(withId(R.id.loadButton)).check(matches(not(isDisplayed())));

        // Do the correct boxes appear for text

        onView(withId(R.id.textRadioButton)).perform(click());

        onView(withId(R.id.textPlaintextData)).check(matches(isDisplayed()));
        onView(withId(R.id.hexPlaintextData)).check(matches(not(isDisplayed())));
        onView(withId(R.id.fileOffset)).check(matches(not(isDisplayed())));
        onView(withId(R.id.filename)).check(matches(not(isDisplayed())));
        onView(withId(R.id.loadButton)).check(matches(not(isDisplayed())));

        onView(withId(R.id.textPlaintextData))
                .perform(typeText("a,c4/f&hIjk Mn p1234567890["),
                         pressImeActionButton());

        onView(withId(R.id.menu_submit)).perform(click());

        onView(withId(R.id.plaintextView))
                .check(matches(withText("61 2C 63 34 2F 66 26 68 49 6A 6B 20 4D 6E 20 70")));

        onView(withId(R.id.plaintextView)).perform(click());

        // Do the correct boxes appear

        onView(withId(R.id.hexRadioButton)).perform(click());

        onView(withId(R.id.textPlaintextData)).check(matches(not(isDisplayed())));
        onView(withId(R.id.hexPlaintextData)).check(matches(isDisplayed()));
        onView(withId(R.id.fileOffset)).check(matches(not(isDisplayed())));
        onView(withId(R.id.filename)).check(matches(not(isDisplayed())));
        onView(withId(R.id.loadButton)).check(matches(not(isDisplayed())));

        // Do the correct boxes appear?

        onView(withId(R.id.fileRadioButton)).perform(click());

        onView(withId(R.id.textPlaintextData)).check(matches(not(isDisplayed())));
        onView(withId(R.id.hexPlaintextData)).check(matches(not(isDisplayed())));
        onView(withId(R.id.fileOffset)).check(matches(not(isDisplayed())));
        onView(withId(R.id.filename)).check(matches(not(isDisplayed())));
        onView(withId(R.id.loadButton)).check(matches(isDisplayed()));


        onView(withId(R.id.hexRadioButton)).perform(click());

        onView(withId(R.id.textPlaintextData)).check(matches(not(isDisplayed())));
        onView(withId(R.id.hexPlaintextData)).check(matches(isDisplayed()));
        onView(withId(R.id.fileOffset)).check(matches(not(isDisplayed())));
        onView(withId(R.id.filename)).check(matches(not(isDisplayed())));
        onView(withId(R.id.loadButton)).check(matches(not(isDisplayed())));
    }

    @Test
    public void testPlaintextHexLimit()
    {
        onView(withId(R.id.plaintextView)).perform(click());
        onView(withId(R.id.hexRadioButton)).perform(click());

        onView(withId(R.id.hexPlaintextData))
                .perform(clearText(),
                         typeText("ac4fhI3le83b18uead"),
                         pressImeActionButton());

        onView(withId(R.id.menu_submit)).perform(click());

        onView(withId(R.id.plaintextView))
                .check(matches(withText("AC 4F 3E 83 B1 8E AD 00 00 00 00 00 00 00 00 00")));

        onView(withId(R.id.plaintextView)).perform(click());

        onView(withId(R.id.hexRadioButton))
                .perform(click());
        onView(withId(R.id.hexPlaintextData))
                .perform(clearText(),
                         typeText("ac4fhIjk Mn p1234567890abefgaloc3cd8b3ac3b20kvce0a3bcek"),
                         pressImeActionButton());

        onView(withId(R.id.menu_submit)).perform(click());

        onView(withId(R.id.plaintextView))
                .check(matches(withText("AC 4F 12 34 56 78 90 AB EF AC 3C D8 B3 AC 3B 20")));

        rotateScreen();

        onView(withId(R.id.plaintextView)).perform(click());
        onView(withId(R.id.hexRadioButton)).perform(click());

        onView(withId(R.id.hexPlaintextData))
                .perform(clearText(),
                         typeText("ac4fhI3le83b18uead"),
                         pressImeActionButton());

        onView(withId(R.id.menu_submit)).perform(click());

        onView(withId(R.id.plaintextView))
                .check(matches(withText("AC 4F 3E 83 B1 8E AD 00 00 00 00 00 00 00 00 00")));

        onView(withId(R.id.plaintextView)).perform(click());

        onView(withId(R.id.hexRadioButton))
                .perform(click());
        onView(withId(R.id.hexPlaintextData))
                .perform(clearText(),
                         typeText("ac4fhIjk Mn p1234567890abefgaloc3cd8b3ac3b20kvce0a3bcek"),
                         pressImeActionButton());

        onView(withId(R.id.menu_submit)).perform(click());

        onView(withId(R.id.plaintextView))
                .check(matches(withText("AC 4F 12 34 56 78 90 AB EF AC 3C D8 B3 AC 3B 20")));
    }

    @Test
    public void testRotate()
    {
        onView(withId(R.id.plaintextView)).perform(click());
        onView(withId(R.id.hexRadioButton)).perform(click());
        onView(withId(R.id.hexPlaintextData))
                .perform(click(),
                         typeText("000102030405060708090a0b0c0d0e0f"),
                         pressImeActionButton());

        onView(withId(R.id.menu_submit)).perform(click());

        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());
        onView(withText(
                InstrumentationRegistry.getTargetContext().getString(R.string.menu_settings)))
                .perform(click());
        onView(withText("Key Length"))
                .perform(click());
        onView(withText("128-bit"))
                .perform(click());

        Espresso.pressBack();

        onView(withId(R.id.key_edittext))
                .perform(click(),
                         typeText("00000000000000000000000000000000"),
                         pressImeActionButton());

        rotateScreen();

        onView(withId(R.id.menu_encrypt)).check(matches(isDisplayed()));
        onView(withId(R.id.menu_submit)).check(doesNotExist());

        onView(withId(R.id.plaintextView))
                .check(matches(withText("00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F")));

        onView(withId(R.id.key_edittext))
                .check(matches(withText("00000000000000000000000000000000")));

        onView(withId(R.id.menu_encrypt)).perform(click());

        onView(withId(R.id.inputGrid)).check(matches(isDisplayed()));
        onView(withId(R.id.ciphertext_textview)).check(matches(isDisplayed()));

        onView(withId(R.id.ciphertext_textview))
                .check(matches(withText("7A CA 0F D9 BC D6 EC 7C 9F 97 46 66 16 E6 A2 82")));
    }

    @Test
    public void testKey128EditSizeChange()
    {
        onView(withId(R.id.plaintextView)).perform(click());
        onView(withId(R.id.hexRadioButton)).perform(click());
        onView(withId(R.id.hexPlaintextData))
                .perform(clearText())
                .perform(click())
                .perform(typeText("000102030405060708090a0b0c0d0e0f"));
        ViewActions.pressImeActionButton();

        onView(withId(R.id.menu_submit)).perform(click());

        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());
        onView(withText(InstrumentationRegistry.getTargetContext()
                                .getString(R.string.menu_settings)))
                .perform(click());
        onView(withText("Key Length"))
                .perform(click());
        onView(withText("128-bit"))
                .perform(click());

        Espresso.pressBack();

        onView(withId(R.id.key_edittext)).perform(click())
                .perform(typeText("000102030405060708"));
        ViewActions.pressImeActionButton();

        onView(withId(R.id.menu_encrypt)).perform(click());


        onView(withId(R.id.key_edittext))
                .check(matches(withText("00010203040506070800000000000000")));

        onView(withId(R.id.key_edittext)).perform(click())
                .perform(clearText());
        ViewActions.pressImeActionButton();

        onView(withId(R.id.menu_encrypt)).perform(click());


        rotateScreen();

        onView(withId(R.id.key_edittext))
                .check(matches(withText("00000000000000000000000000000000")));

        onView(withId(R.id.key_edittext))
                .perform(clearText(), typeText("000102030405060708090a0b0c0d0e0f1011121314151617"),
                         pressImeActionButton());

        onView(withId(R.id.menu_encrypt)).perform(click());


        onView(withId(R.id.key_edittext))
                .check(matches(withText("000102030405060708090A0B0C0D0E0F")));

        rotateScreen();
        Espresso.closeSoftKeyboard();

        onView(withId(R.id.plaintextView)).perform(click());
        onView(withId(R.id.hexRadioButton)).perform(click());
        onView(withId(R.id.hexPlaintextData))
                .perform(clearText(),
                         typeText("000102030405060708090a0b0c0d0e0f"),
                         pressImeActionButton());

        onView(withId(R.id.menu_submit)).perform(click());

        rotateScreen();
        rotateScreen();

        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());
        onView(withText(InstrumentationRegistry.getTargetContext()
                                .getString(R.string.menu_settings)))
                .perform(click());
        onView(withText("Key Length"))
                .perform(click());
        onView(withText("128-bit"))
                .perform(click());

        Espresso.pressBack();

        onView(withId(R.id.key_edittext))
                .perform(clearText(),
                         typeText("000102030405060708"),
                         pressImeActionButton());

        onView(withId(R.id.menu_encrypt)).perform(click());


        onView(withId(R.id.key_edittext))
                .check(matches(withText("00010203040506070800000000000000")));
        rotateScreen();
        Espresso.closeSoftKeyboard();

        onView(withId(R.id.key_edittext))
                .perform(click(),
                         clearText(),
                         pressImeActionButton());

        onView(withId(R.id.menu_encrypt)).perform(click());


        onView(withId(R.id.key_edittext))
                .check(matches(withText("00000000000000000000000000000000")));

        rotateScreen();
        rotateScreen();
        onView(withId(R.id.key_edittext)).perform(click())
                .perform(clearText());
        rotateScreen();
        onView(withId(R.id.key_edittext))
                .perform(clearText(),
                         typeText("000102030405060708090a0b0c0d0e0f1011121314151617"));

        Espresso.closeSoftKeyboard();
        onView(withId(R.id.menu_encrypt)).perform(click());


        onView(withId(R.id.key_edittext))
                .check(matches(withText("000102030405060708090A0B0C0D0E0F")));
    }

    @Test
    public void testKey192EditSizeChange()
    {
        onView(withId(R.id.plaintextView)).perform(click());
        onView(withId(R.id.hexRadioButton)).perform(click());
        onView(withId(R.id.hexPlaintextData))
                .perform(clearText(),
                         typeText("000102030405060708090a0b0c0d0e0f"),
                         pressImeActionButton());

        onView(withId(R.id.menu_submit)).perform(click());

        rotateScreen();

        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());
        onView(withText(
                InstrumentationRegistry.getTargetContext().getString(R.string.menu_settings)))
                .perform(click());
        onView(withText("Key Length"))
                .perform(click());
        onView(withText("192-bit"))
                .perform(click());

        Espresso.pressBack();

        onView(withId(R.id.key_edittext))
                .perform(clearText(),
                         typeText("000102030405060708"),
                         pressImeActionButton());

        onView(withId(R.id.menu_encrypt)).perform(click());


        rotateScreen();
        onView(withId(R.id.key_edittext))
                .check(matches(withText("000102030405060708000000000000000000000000000000")));
        onView(withId(R.id.key_edittext))
                .perform(clearText());

        rotateScreen();

        onView(withId(R.id.menu_encrypt)).perform(click());


        onView(withId(R.id.key_edittext))
                .check(matches(withText("000000000000000000000000000000000000000000000000")));

        onView(withId(R.id.key_edittext)).perform(click())
                .perform(clearText());
        rotateScreen();

        onView(withId(R.id.key_edittext))
                .perform(clearText(),
                         typeText(
                                 "000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f"));
Espresso.closeSoftKeyboard();
        onView(withId(R.id.menu_encrypt)).perform(click());


        onView(withId(R.id.key_edittext))
                .check(matches(withText(
                        "000102030405060708090a0b0c0d0e0f1011121314151617".toUpperCase())));

        rotateScreen();

        onView(withId(R.id.plaintextView)).perform(click());
        onView(withId(R.id.hexRadioButton)).perform(click());
        onView(withId(R.id.hexPlaintextData))
                .perform(clearText(),
                         typeText("000102030405060708090a0b0c0d0e0f"),
                         pressImeActionButton());

        onView(withId(R.id.menu_submit)).perform(click());
        rotateScreen();
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());
        onView(withText(
                InstrumentationRegistry.getTargetContext().getString(R.string.menu_settings)))
                .perform(click());
        rotateScreen();
        onView(withText("Key Length"))
                .perform(click());
        onView(withText("192-bit"))
                .perform(click());

        Espresso.pressBack();

        onView(withId(R.id.key_edittext))
                .perform(clearText(),
                         typeText("000102030405060708"));
        rotateScreen();
        rotateScreen();
        Espresso.closeSoftKeyboard();

        onView(withId(R.id.menu_encrypt)).perform(click());


        rotateScreen();
        onView(withId(R.id.key_edittext))
                .check(matches(withText("000102030405060708000000000000000000000000000000")));
        onView(withId(R.id.key_edittext))
                .perform(clearText(),
                         pressImeActionButton());

        onView(withId(R.id.key_edittext))
                .check(matches(withText("000000000000000000000000000000000000000000000000")));

        onView(withId(R.id.key_edittext)).perform(clearText(),
                                                  pressImeActionButton());
        rotateScreen();
        onView(withId(R.id.key_edittext))
                .perform(clearText(),
                         typeText(
                                 "000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f"),
                         pressImeActionButton());

        onView(withId(R.id.key_edittext))
                .check(matches(withText(
                        "000102030405060708090a0b0c0d0e0f1011121314151617".toUpperCase())));
    }

    @Test
    public void testKey256EditSizeChange()
    {
        onView(withId(R.id.plaintextView)).perform(click());
        onView(withId(R.id.hexRadioButton)).perform(click());
        onView(withId(R.id.hexPlaintextData))
                .perform(clearText(),
                         typeText("000102030405060708090a0b0c0d0e0f"),
                         pressImeActionButton());

        onView(withId(R.id.menu_submit)).perform(click());
        rotateScreen();
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());
        onView(withText(InstrumentationRegistry.getTargetContext()
                                .getString(R.string.menu_settings)))
                .perform(click());
        onView(withText("Key Length"))
                .perform(click());
        onView(withText("256-bit"))
                .perform(click());
        rotateScreen();
        Espresso.pressBack();

        onView(withId(R.id.key_edittext))
                .perform(clearText(), typeText("000102030405060708"),
                         pressImeActionButton());

        rotateScreen();
        rotateScreen();
        onView(withId(R.id.key_edittext))
                .check(matches(withText(
                        "0001020304050607080000000000000000000000000000000000000000000000")));

        onView(withId(R.id.key_edittext)).perform(clearText());
        Espresso.closeSoftKeyboard();
        onView(withId(R.id.menu_encrypt)).perform(click());


        rotateScreen();
        onView(withId(R.id.key_edittext))
                .check(matches(withText(
                        "0000000000000000000000000000000000000000000000000000000000000000")));

        onView(withId(R.id.key_edittext)).perform(clearText(), pressImeActionButton());
        rotateScreen();
        onView(withId(R.id.key_edittext))
                .perform(clearText(),
                         typeText("000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f2021222324252627"),
                         pressImeActionButton());

        onView(withId(R.id.menu_encrypt)).perform(click());


        rotateScreen();
        rotateScreen();
        onView(withId(R.id.key_edittext))
                .check(matches(withText(
                        "000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f"
                                .toUpperCase())));
    }

    @Test
    public void testEncrypt()
    {
        onView(withId(R.id.plaintextView)).perform(click());
        onView(withId(R.id.hexRadioButton)).perform(click());
        onView(withId(R.id.hexPlaintextData))
                .perform(click(),
                         typeText("000102030405060708090a0b0c0d0e0f"),
                         pressImeActionButton());

        onView(withId(R.id.menu_submit)).perform(click());

        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());
        onView(withText(
                InstrumentationRegistry.getTargetContext().getString(R.string.menu_settings)))
                .perform(click());
        onView(withText("Key Length"))
                .perform(click());
        onView(withText("128-bit"))
                .perform(click());

        Espresso.pressBack();

        onView(withId(R.id.key_edittext))
                .perform(click(),
                         typeText("00000000000000000000000000000000"),
                         pressImeActionButton());

        onView(withId(R.id.plaintextView))
                .check(matches(withText("00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F")));

        onView(withId(R.id.menu_encrypt)).perform(click());


        onView(withId(R.id.gridScrollView)).check(matches(isDisplayed()));
        onView(withId(R.id.ciphertextLayout)).check(matches(isDisplayed()));

        onView(withId(R.id.ciphertext_textview))
                .check(matches(withText("7A CA 0F D9 BC D6 EC 7C 9F 97 46 66 16 E6 A2 82")));
    }

    @Test
    public void testAboutVersion()
    {
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getTargetContext());
        onView(withText(InstrumentationRegistry.getTargetContext().getString(R.string.menu_about)))
                .perform(click());

        onView(withId(R.id.appVersion)).check(matches(withText(BuildConfig.VERSION_NAME)));
    }
}
