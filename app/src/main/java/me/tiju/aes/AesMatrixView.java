package me.tiju.aes;

import android.content.Context;
import android.graphics.Typeface;

import androidx.core.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.Locale;

public class AesMatrixView extends TableLayout
{
    protected boolean hasColumnHeader;
    protected boolean hasRowHeader;

    public AesMatrixView(Context context)
    {
        super(context);
        init(4, 4);
    }

    public AesMatrixView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        final String projectNs = "http://schemas.android.com/apk/res-auto";

        hasColumnHeader = attrs.getAttributeBooleanValue(projectNs, "colHeader", false);
        hasRowHeader = attrs.getAttributeBooleanValue(projectNs, "rowHeader", false);

        init(attrs.getAttributeIntValue(projectNs, "rows", 4),
             attrs.getAttributeIntValue(projectNs, "cols", 4));
    }

    private void init(int rows, int cols)
    {
        TableRow tableRow;
        TextView textView;

        if(rows <= 0)
            throw new IllegalArgumentException("Number of rows must be positive");

        if(cols <= 0)
            throw new IllegalArgumentException("Number of cols must be positive");

        final int minEms=getContext().getResources().getInteger(R.integer.matrixMinEms);
        final int maxEms=getContext().getResources().getInteger(R.integer.matrixMaxEms);

        for(int rowIndex=0; rowIndex < rows + (hasColumnHeader ? 1 : 0); rowIndex++)
        {
            tableRow = new TableRow(getContext());

            for (int colIndex = 0; colIndex < cols + (hasRowHeader ? 1 : 0); colIndex++)
            {
                textView = new TextView(getContext());
                textView.setMinEms(minEms);
                textView.setMaxEms(maxEms);
                textView.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);

                if((hasRowHeader && colIndex == 0)
                        || (hasColumnHeader && rowIndex == 0))
                {
                    textView.setTypeface(textView.getTypeface(), Typeface.BOLD);
                }

                tableRow.addView(textView);
            }
            addView(tableRow);

            tableRow.getLayoutParams().width = TableRow.LayoutParams.MATCH_PARENT;
            tableRow.getLayoutParams().height = TableRow.LayoutParams.WRAP_CONTENT;
        }

        invalidate();
    }

    public void setValues(int[] values)
    {
        int numRows = getChildCount();
        int numCols = (numRows > 0 ? ((TableRow)getChildAt(0)).getChildCount() : 0);
        int i=0;

        for(int row=(hasColumnHeader ? 1 : 0); row < numRows; row++)
        {
            for(int col=(hasRowHeader ? 1 : 0); col < numCols; col++)
            {
                TableRow tableRow = (TableRow) getChildAt(row);
                TextView textView = (TextView) tableRow.getChildAt(col);

                if ((hasRowHeader && col == 0)
                    || (hasColumnHeader && row == 0))
                {
                    textView.setBackgroundColor(ResourcesCompat.getColor(getResources(),
                                                                         R.color.checkeredDark,
                                                                         getContext().getTheme()));
                }
                else
                {
                    textView.setBackgroundColor(
                            ((row + (col*(numRows-(hasColumnHeader?1:0)))) % 2) == 0
                                    ? ResourcesCompat.getColor(getResources(),
                                                               R.color.checkeredDark, getContext().getTheme())
                                    : ResourcesCompat.getColor(getResources(),
                                                               R.color.checkeredLight, getContext().getTheme()));

                    if(((row + (col*(numRows-(hasColumnHeader?1:0)))) % 2) != 0)
                    textView.setTextColor(
                             ResourcesCompat.getColor(getResources(),
                                                      R.color.textOnCheckeredLight, getContext().getTheme()));
                }
                textView.setText(String.format(Locale.getDefault(), "%02X", values[i++]));
            }
        }
    }

    public void setRowHeader(int[] header)
    {
        if (!hasRowHeader)
            return;

        for (int i = 0; i < header.length && i < getChildCount(); i++)
        {
            TextView textView = ((TextView) ((TableRow) getChildAt(i+1)).getChildAt(0));
            textView.setText(String.format(Locale.getDefault(), "%02X", header[i]));
            textView.setTypeface(textView.getTypeface(), Typeface.BOLD);
        }
    }

    public void setColHeader(int[] header)
    {
        if (!hasColumnHeader)
            return;

        TableRow rowHeader = (TableRow) getChildAt(0);

        for (int i = 0; i < header.length && i < rowHeader.getChildCount(); i++)
        {
            TextView textView = ((TextView) rowHeader.getChildAt(i+1));
            textView.setText(String.format(Locale.getDefault(), "%02X", header[i]));
            textView.setTypeface(textView.getTypeface(), Typeface.BOLD);
        }
    }
}
