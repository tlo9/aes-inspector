package me.tiju.aes;

import androidx.fragment.app.Fragment;

import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SboxFragment extends Fragment
{
    public SboxFragment()
    {
        // Required empty public constructor
    }

    @NonNull
    static SboxFragment newInstance()
    {
        return new SboxFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_sbox, container, false);
        Resources resources = inflater.getContext().getResources();

        final AesMatrixView sboxMatrix = v.findViewById(R.id.sbox_matrix);
        final AesMatrixView inverseSboxMatrix = v.findViewById(R.id.inv_sbox_matrix);

        int[] sboxValues = resources.getIntArray(R.array.rijndael_sbox);
        int[] invSboxValues = resources.getIntArray(R.array.rijndael_inv_sbox);
        int[] colHeader = resources.getIntArray(R.array.column_header);
        int[] rowHeader = resources.getIntArray(R.array.row_header);

        for(int rows=0; rows < 16; rows++)
        {
            for(int cols=rows; cols < 16; cols++)
            {
                int j = sboxValues[rows*16+cols];
                sboxValues[rows*16+cols] = sboxValues[cols*16+rows];
                sboxValues[cols*16+rows] = j;

                int k = invSboxValues[rows*16+cols];
                invSboxValues[rows*16+cols] = invSboxValues[cols*16+rows];
                invSboxValues[cols*16+rows] = k;
            }
        }

        sboxMatrix.setColHeader(colHeader);
        sboxMatrix.setRowHeader(rowHeader);
        sboxMatrix.setValues(sboxValues);

        inverseSboxMatrix.setColHeader(colHeader);
        inverseSboxMatrix.setRowHeader(rowHeader);
        inverseSboxMatrix.setValues(invSboxValues);
        return v;
    }
}
