package me.tiju.aes;

import android.content.Context;

public interface LifecycleListener
{
    void onCreate(Context context);
    void onDestroy(Context context);
    void onPause(Context context);
    void onResume(Context context);
    void onStop(Context context);
    void onStart(Context context);
}
