package me.tiju.aes;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;

public class LegaleseActivity extends AppCompatActivity
{
    public static final String PRIVACY = "privacy";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legalese);
        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        Spanned spannedText;
        String filename = null;

        String path = null;

        try
        {
            if (intent == null)
                finish();
            else if (intent.getData() == null || intent.getData().getPathSegments()
                                                         .size() != 2)
                finish();

            if (intent != null)
                path = intent.getData().getPathSegments().get(1);
        }
        catch (NullPointerException e)
        {
            finish();
        }

        if (path == null)
            finish();
        else
        {
            if (path.toLowerCase().equalsIgnoreCase(PRIVACY))
            {
                filename = "privacy.html";
                toolbar.setTitle(getString(R.string.legalese_privacy));
            }
            else
                finish();

            try
            {
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(getAssets()
                                                      .open(String.format(
                                                              Locale.getDefault(),
                                                              "%s",
                                                              filename))));
                StringBuilder strBuilder = new StringBuilder();
                String line;

                do
                {
                    line = reader.readLine();

                    if (line != null)
                    {
                        strBuilder.append(line);
                        strBuilder.append('\n');
                    }
                } while (line != null);

                reader.close();

                if (Build.VERSION.SDK_INT < 24)
                    spannedText = Html.fromHtml(strBuilder.toString());
                else
                {
                    spannedText = Html.fromHtml(strBuilder.toString(),
                                                Html.FROM_HTML_SEPARATOR_LINE_BREAK_BLOCKQUOTE
                                                | Html.FROM_HTML_SEPARATOR_LINE_BREAK_HEADING
                                                | Html.FROM_HTML_SEPARATOR_LINE_BREAK_LIST);
                }

                TextView content = findViewById(R.id.content);

                content.setMovementMethod(LinkMovementMethod.getInstance());
                content.setText(spannedText);
                content.setLinkTextColor(
                        ResourcesCompat.getColor(getResources(), R.color.link, getTheme()));
            }
            catch (IOException e)
            {
                Log.e("IOException", e.toString());
                finish(); // Try loading the web version instead
            }
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    @Override
    public void onDestroy()
    {
        TextView content = findViewById(R.id.content);

        setSupportActionBar(null);
        content.setMovementMethod(null);

        super.onDestroy();
    }
}
