package me.tiju.aes;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.google.ads.consent.ConsentForm;
import com.google.ads.consent.ConsentFormListener;
import com.google.ads.consent.ConsentInfoUpdateListener;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.ads.consent.DebugGeography;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import androidx.preference.PreferenceManager;

public class VisualizationActivity extends AppCompatActivity implements FragmentChangeListener,
        DismissListener, PlaintextFragment.PlaintextListener
{
    final public static String VISUALIZATION_FRAGMENT = "visualizationFragment";
    final private static String SBOX_FRAGMENT = "sboxFragment";
    final private static String SETTINGS_FRAGMENT = "settingsFragment";
    final private static String ABOUT_FRAGMENT = "aboutFragment";
    final private static String PLAINTEXT_FRAGMENT = "plaintextFragment";
    final private static String PLAINTEXT = "plaintextStr";

    private VisualizationFragment visualizationFragment;
    private PlaintextFragment plaintextFragment;
    private AboutFragment aboutFragment;
    private SboxFragment sboxFragment;
    private SettingsFragment settingsFragment;
    private String plaintextStr;
    private Intent privacyIntent;
    private ConsentForm mConsentForm;
    private int adId;
    private GoogleAdManager adManager;
    private boolean showAds=true;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                             WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.activity_visualization);

        final boolean firstRun = savedInstanceState == null;

        if (privacyIntent == null)
        {
            privacyIntent = new Intent(Intent.ACTION_VIEW,
                                       Uri.parse(String.format(Locale.ENGLISH,
                                                               "view://%s/legal/%s",
                                                               BuildConfig.APPLICATION_ID, LegaleseActivity.PRIVACY)));
            privacyIntent.setClass(getApplicationContext(), LegaleseActivity.class);
        }

        FragmentManager fm = getSupportFragmentManager();

        if (savedInstanceState != null)
        {
            visualizationFragment = (VisualizationFragment) fm
                    .getFragment(savedInstanceState, VISUALIZATION_FRAGMENT);
            sboxFragment = (SboxFragment) fm
                    .getFragment(savedInstanceState, SBOX_FRAGMENT);
            settingsFragment = (SettingsFragment) fm
                    .getFragment(savedInstanceState, SETTINGS_FRAGMENT);
            plaintextFragment = (PlaintextFragment) fm
                    .getFragment(savedInstanceState, PLAINTEXT_FRAGMENT);

            setPlaintext(savedInstanceState.getString(PLAINTEXT));
        }
        else
        {
            visualizationFragment = VisualizationFragment.newInstance();

            if (!fm.isStateSaved())
            {
                fm.beginTransaction()
                        .setCustomAnimations(android.R.animator.fade_in,
                                             android.R.animator.fade_out,
                                             android.R.animator.fade_in, android.R.animator.fade_out)
                        .add(R.id.activity_container, visualizationFragment, VISUALIZATION_FRAGMENT)
                        .addToBackStack(null)
                        .commit();
            }
        }

        if (!BuildConfig.DEBUG || showAds)
        {
            AdEventListener adEventListener = new AdEventListener() {
                @Override
                public void onAdOpened(int adId)
                {

                }

                @Override
                public void onAdClosed(int adId)
                {

                }

                @Override
                public void onAdLoaded(int adId)
                {

                }

                @Override
                public void onAdError(int adId, int code)
                {

                }

                @Override
                public void onAdClicked(int adId)
                {

                }

                @Override
                public void onAdScheduleReload(int adId, long delay)
                {

                }
            };

            adManager = new GoogleAdManager(getApplicationContext(), adEventListener);
            adId = adManager.createAd(AdManager.AdType.BANNER);
            adManager.onCreate(this);

            View v = (View) adManager.getAdObject(adId);

            if (v != null)
            {
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                ((ViewGroup) findViewById(R.id.ad_container)).addView(v, params);
                findViewById(R.id.main_container).requestLayout();
            }
        }


        final SharedPreferences sharedPrefs = getSharedPreferences(getString(R.string.main_prefs), Context.MODE_PRIVATE);
        boolean askedForConsent = sharedPrefs.getBoolean(getString(R.string.pref_asked_consent), false);

        if(BuildConfig.DEBUG || !askedForConsent)
        {
            final SharedPreferences preferences = PreferenceManager
                    .getDefaultSharedPreferences(this);
            ConsentInformation consentInformation = ConsentInformation.getInstance(this);
            String[] publisherIds = getResources().getStringArray(R.array.publisher_ids);

            if(BuildConfig.DEBUG && firstRun)
                consentInformation.setDebugGeography(DebugGeography.DEBUG_GEOGRAPHY_EEA);
            consentInformation
                    .requestConsentInfoUpdate(publisherIds, new ConsentInfoUpdateListener()
                    {
                        @Override
                        public void onConsentInfoUpdated(ConsentStatus consentStatus)
                        {
                            // User's consent status successfully updated.

                            boolean hasConsent = consentStatus != ConsentStatus.NON_PERSONALIZED;

                            // Consent form was closed.

                            preferences
                                    .edit()
                                    .putBoolean(getString(R.string.pref_ad_consent), hasConsent)
                                    .putBoolean(getString(R.string.pref_analytics_consent),
                                                hasConsent)
                                    .apply();
                        }

                        @Override
                        public void onFailedToUpdateConsentInfo(String errorDescription)
                        {
                            // User's consent status failed to update.
                        }
                    });

            URL privacyUrl = null;
            try
            {
                privacyUrl = new URL(getString(R.string.privacy_url));
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
            }

            final boolean isInEu = BuildConfig.DEBUG || consentInformation
                    .isRequestLocationInEeaOrUnknown();

            mConsentForm = new ConsentForm.Builder(this, privacyUrl)
                    .withListener(new ConsentFormListener()
                    {
                        @Override
                        public void onConsentFormLoaded()
                        {
                            // Consent form loaded successfully.

                            if (mConsentForm != null && isInEu)
                                mConsentForm.show();
                        }

                        @Override
                        public void onConsentFormOpened()
                        {
                            // Consent form was displayed.
                        }

                        @Override
                        @SuppressLint("ApplySharedPref")
                        public void onConsentFormClosed(
                                ConsentStatus consentStatus, Boolean userPrefersAdFree)
                        {
                            boolean hasConsent = consentStatus != ConsentStatus.NON_PERSONALIZED;

                            // Consent form was closed.

                            preferences
                                    .edit()
                                    .putBoolean(getString(R.string.pref_ad_consent), hasConsent)
                                    .putBoolean(getString(R.string.pref_analytics_consent),
                                                hasConsent)
                                    .commit();

                            sharedPrefs
                                    .edit()
                                    .putBoolean(getString(R.string.pref_asked_consent), true)
                                    .apply();

                            initAdsAndAnalytics(firstRun);
                        }

                        @Override
                        public void onConsentFormError(String errorDescription)
                        {
                            initAdsAndAnalytics(firstRun);
                        }
                    })
                    .withPersonalizedAdsOption()
                    .withNonPersonalizedAdsOption()
                    .build();

            mConsentForm.load();
        }
        else
            initAdsAndAnalytics(firstRun);
    }

    private void initAdsAndAnalytics(boolean firstRun)
    {
        boolean hasAnalyticsConsent = PreferenceManager.getDefaultSharedPreferences(this)
                .getBoolean(getString(R.string.pref_analytics_consent), true);
        FirebaseAnalytics analytics = FirebaseAnalytics.getInstance(this);

        analytics.setAnalyticsCollectionEnabled(hasAnalyticsConsent);

        if(!hasAnalyticsConsent)
            analytics.resetAnalyticsData();

        if(firstRun)
            MobileAds.initialize(this, getString(R.string.admob_app_id));

        if(adManager != null)
            adManager.loadAd(adId);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState)
    {
        super.onSaveInstanceState(outState);

        if(plaintextStr != null)
            outState.putString(PLAINTEXT, plaintextStr);
    }

    @Override
    public void onDismiss()
    {
        goBack();
    }

    @Override
    public void goToAbout()
    {
        if(aboutFragment == null)
            aboutFragment = AboutFragment.newInstance();

        hideKeyboard();
        FragmentManager fm = getSupportFragmentManager();

        if(!fm.isStateSaved())
        {
            fm.beginTransaction()
                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out,
                                         android.R.animator.fade_in, android.R.animator.fade_out)
                    .replace(R.id.activity_container, aboutFragment, ABOUT_FRAGMENT)
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public void goToSbox()
    {
        if(sboxFragment == null)
            sboxFragment = SboxFragment.newInstance();

        hideKeyboard();

        FragmentManager fm = getSupportFragmentManager();

        if(!fm.isStateSaved())
        {
            fm.beginTransaction()
                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out,
                                         android.R.animator.fade_in, android.R.animator.fade_out)
                    .replace(R.id.activity_container, sboxFragment, SBOX_FRAGMENT)
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public void goToSettings()
    {
        if(settingsFragment == null)
            settingsFragment = SettingsFragment.newInstance();

        hideKeyboard();

        FragmentManager fm = getSupportFragmentManager();

        if(!fm.isStateSaved())
        {
            fm.beginTransaction()
                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out,
                                         android.R.animator.fade_in, android.R.animator.fade_out)
                    .replace(R.id.activity_container, settingsFragment, SETTINGS_FRAGMENT)
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public void goToPlaintextEdit()
    {
        if(plaintextFragment == null)
            plaintextFragment = PlaintextFragment.newInstance(plaintextStr);

        hideKeyboard();

        FragmentManager fm = getSupportFragmentManager();

        if(!fm.isStateSaved())
        {
            fm.beginTransaction()
                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out,
                                         android.R.animator.fade_in, android.R.animator.fade_out)
                    .replace(R.id.activity_container, plaintextFragment, PLAINTEXT_FRAGMENT)
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public void goBack()
    {
        hideKeyboard();

        getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onBackPressed()
    {
        FragmentManager fragmentManager = getSupportFragmentManager();

        if((visualizationFragment != null && visualizationFragment.isVisible())
           || fragmentManager.getBackStackEntryCount() <= 1)
            finish();
        else
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_visualization, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        Bundle bundle;

        switch(item.getItemId())
        {
            case R.id.menu_privacy:
                bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "button");
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "privacy");
                FirebaseAnalytics.getInstance(this).logEvent("select_content", bundle);

                if(privacyIntent != null)
                    startActivity(privacyIntent);

                return true;
            case R.id.menu_about:
                bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "button");
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "about");
                FirebaseAnalytics.getInstance(this).logEvent("select_content", bundle);
                goToAbout();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart()
    {
        super.onStart();

        if(adManager != null)
            adManager.onStart(this);
    }

    @Override
    public void onStop()
    {
        super.onStop();

        if(adManager != null)
            adManager.onStop(this);
    }

    @Override
    public void onDestroy()
    {
        if(adManager != null)
        {
            adManager.destroyAd(adId);
            adManager.onDestroy(this);
        }

        visualizationFragment = null;
        plaintextFragment = null;
        aboutFragment = null;
        sboxFragment = null;
        settingsFragment = null;
        privacyIntent = null;
        mConsentForm = null;
        super.onDestroy();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if(adManager != null)
            adManager.onResume(this);
    }

    @Override
    public void onPause()
    {
        super.onPause();

        if(adManager != null)
            adManager.onPause(this);
    }

    @Override
    public void hideKeyboard()
    {
        InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);

        if(imm != null)
            imm.hideSoftInputFromWindow(findViewById(R.id.activity_container).getWindowToken(),
                                    InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public void setPlaintext(String plaintext)
    {
        plaintextStr = plaintext;
    }

    @Override
    public String getPlaintext()
    {
        return plaintextStr;
    }
}
