package me.tiju.aes;

import android.content.Context;
import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

final class Rijndael
{
    final static int AES_BLOCK_SIZE = 16; // AES block size in bytes
    private static int[] tempCol = new int[4];
    private static int[] tempBlock = new int[AES_BLOCK_SIZE];

    @CheckResult
    private static int add(int... bytes)
    {
        int result = 0;

        for(int b : bytes)
        {
            b &= 0xff;
            result ^= (b & 0xff);
        }

        return result;
    }

    @CheckResult
    private static int multiply(@NonNull Context context, int... bytes)
    {
        int e = 0;
        final int[] G = context.getResources().getIntArray(R.array.rijndael_gen);
        final int[] LOG = context.getResources().getIntArray(R.array.rijndael_log);

        for (int b : bytes)
        {
            b &= 0xff;

            if (b == 0)
                return 0;
            else
                e += LOG[b];
        }

        return G[e%255];
    }
/*
    public static int exp(@NonNull Context context, int a, int e)
    {
        final int[] G = context.getResources().getIntArray(R.array.rijndael_gen);
        final int[] LOG = context.getResources().getIntArray(R.array.rijndael_log);

        a &= 0xff;
        e &= 0xff;

        if(a == 0)
            return 0;
        else
            return G[(e*LOG[a])%255];
    }
*/

    static @Nullable int[] expandKey(@NonNull Context context, int[] reusedKey,
                                            @NonNull int[] key, int keyLength)
    {
        int[] expandedKey;
        final int[] SBOX = context.getResources().getIntArray(R.array.rijndael_sbox);
        final int[] RCON = context.getResources().getIntArray(R.array.rijndael_rcon);
        final int rounds = getRounds(keyLength);

        if(rounds == 0)
            throw new IllegalArgumentException("Invalid key size");

        if(reusedKey == null || reusedKey.length < (1+rounds)*AES_BLOCK_SIZE)
            expandedKey = new int[(1+rounds)*AES_BLOCK_SIZE];
        else
            expandedKey = reusedKey;

        System.arraycopy(key, 0, expandedKey, 0, key.length);

        switch(keyLength)
        {
            case 128:
                for(int i=1; i < 1+rounds; i++)
                {
                    for(int j=0; j < 4; j++)
                    {
                        tempCol[j] = add(SBOX[expandedKey[AES_BLOCK_SIZE*i+(j+1)%4-4]],
                                              expandedKey[AES_BLOCK_SIZE*(i-1)+j], j == 0 ? RCON[i] : 0);
                    }

                    System.arraycopy(tempCol, 0, expandedKey, AES_BLOCK_SIZE*i, 4);

                    for(int j=1; j < 4; j++)
                    {
                        for (int k=0; k < 4; k++)
                        {
                            expandedKey[AES_BLOCK_SIZE*i+4*j+k] = add(expandedKey[AES_BLOCK_SIZE*i+4*(j-1)+k],
                                        expandedKey[AES_BLOCK_SIZE*(i-1)+4*j+k]);
                        }
                    }
                }
                break;
            case 192:
                for(int i=6; i < 52; i++)
                {
                    if(i % 6 == 0)
                    {
                        for (int j=0; j < 4; j++)
                        {
                            tempCol[j] = add(SBOX[expandedKey[4 * (i-1) + (j + 1) % 4]],
                                          expandedKey[4 * (i - 6) + j], j == 0 ? RCON[i/6] : 0);
                        }

                        System.arraycopy(tempCol, 0, expandedKey, 4*i, 4);
                    }
                    else
                    {
                        for(int j=0; j < 4; j++)
                            expandedKey[4*i+j] = add(expandedKey[4*(i-1)+j],
                                                     expandedKey[4*(i-6)+j]);
                    }
                }
                break;
            case 256:
                for(int i=8; i < 60; i++)
                {
                    if(i % 4 == 0)
                    {
                        if(i % 8 == 0)
                        {
                            for (int j=0; j < 4; j++)
                                tempCol[j] = add(SBOX[expandedKey[4*(i-1)+(j+1)%4]],
                                              expandedKey[4*(i-8)+j], j == 0 ? RCON[i/8] : 0);
                        }
                        else
                        {
                            for(int j=0; j < 4; j++)
                                tempCol[j] = add(SBOX[expandedKey[4 * (i - 1) + j]],
                                              expandedKey[4 * (i - 8) + j]);
                        }

                        System.arraycopy(tempCol, 0, expandedKey, 4*i, 4);
                    }
                    else
                    {
                        for(int j=0; j < 4; j++)
                            expandedKey[4*i+j] = add(expandedKey[4*(i-1)+j],
                                                     expandedKey[4*(i-8)+j]);
                    }
                }
                break;
            default:
                return null;
        }

        return expandedKey;
    }

    @CheckResult
    static int[] subBytes(@NonNull Context context, @NonNull int[] inBlock, int[] outBlock)
    {
        final int[] SBOX = context.getResources().getIntArray(R.array.rijndael_sbox);

        System.arraycopy(inBlock, 0, tempBlock, 0, AES_BLOCK_SIZE);

        if(outBlock == null || outBlock.length != AES_BLOCK_SIZE)
            outBlock = new int[AES_BLOCK_SIZE];

        for (int i = 0; i < AES_BLOCK_SIZE; i++)
            outBlock[i] = SBOX[tempBlock[i]];

        return outBlock;
    }

    @CheckResult
    static int[] shiftRows(@NonNull int[] inBlock, int[] outBlock)
    {
        System.arraycopy(inBlock, 0, tempBlock, 0, AES_BLOCK_SIZE);

        if(outBlock == null || outBlock.length != AES_BLOCK_SIZE)
            outBlock = new int[AES_BLOCK_SIZE];

        for (int col = 0; col < 4; col++)
            for (int row = 0; row < 4; row++)
                outBlock[4 * col + row] = tempBlock[4 * ((col + row) % 4) + row];

        return outBlock;
    }

    @CheckResult
    static int[] mixColumns(@NonNull Context context, @NonNull int[] inBlock, int round,
                            int maxRounds, int[] outBlock)
    {
        final int[] MATRIX = context.getResources().getIntArray(R.array.rijndael_mix_columns);

        if(outBlock == null || outBlock.length != AES_BLOCK_SIZE)
            outBlock = new int[AES_BLOCK_SIZE];

        if(round < maxRounds)
        {
            System.arraycopy(inBlock, 0, tempBlock, 0, AES_BLOCK_SIZE);

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    outBlock[4 * i + j] = 0;

                    for (int k = 0; k < 4; k++)
                        outBlock[4 * i + j] = add(
                                multiply(context, tempBlock[4 * i + k], MATRIX[4 * k + j]),
                                outBlock[4 * i + j]);
                }
            }
        }

        return outBlock;
    }

    @CheckResult
    static int[] addRoundKey(@NonNull int[] inBlock, int round, @NonNull int[] expandedKey,
                             int[] outBlock)
    {
        if(outBlock == null || outBlock.length != AES_BLOCK_SIZE)
            outBlock = new int[AES_BLOCK_SIZE];

        System.arraycopy(inBlock, 0, outBlock, 0, AES_BLOCK_SIZE);

        for(int i=0; i < AES_BLOCK_SIZE; i++)
            outBlock[i] = add(outBlock[i], expandedKey[round*AES_BLOCK_SIZE+i]);

        return outBlock;
    }

    static @Nullable @CheckResult int[] encrypt(@NonNull Context context, @NonNull int[] key,
                                          @NonNull int[] plaintext, int[] output)
    {
        int[] ciphertext;
        int[] expandedKey;
        int rounds = getRounds(key.length*8);

        if(rounds == 0)
            throw new IllegalArgumentException("Invalid key length");

        if(plaintext.length < AES_BLOCK_SIZE)
            throw new IllegalArgumentException("Plaintext should be at least one block in length");

        expandedKey = expandKey(context, null, key, key.length * 8);

        if(expandedKey == null)
            return null;

        if(output == null || output.length < AES_BLOCK_SIZE)
            ciphertext = new int[AES_BLOCK_SIZE];
        else
            ciphertext = output;

        System.arraycopy(plaintext, 0, ciphertext, 0, AES_BLOCK_SIZE);

        ciphertext = addRoundKey(ciphertext, 0, expandedKey, ciphertext);

        for(int round=1; round <= rounds; round++)
        {
            ciphertext = addRoundKey(mixColumns(context, shiftRows(subBytes(context, ciphertext, ciphertext), ciphertext),
                                   round, rounds, ciphertext), round, expandedKey, ciphertext);
        }

        return ciphertext;
    }

    @CheckResult
    static int getRounds(int keyLength)
    {
        switch(keyLength)
        {
            case 128:
                return 10;
            case 192:
                return 12;
            case 256:
                return 14;
            default:
                return 0;
        }
    }
}
