package me.tiju.aes;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.Locale;
import java.util.regex.Pattern;

import androidx.fragment.app.Fragment;

public class VisualizationFragment extends Fragment
{
    final private static String KEY = "key";
    final private static String KEY_LEN = "keyLength";
    final private static String HIDDEN_STATE = "hiddenState";
    final private static String EXPANDED_KEY = "expandedKey";
    private final static int DEFAULT_KEY_LENGTH = 128;
    private int keyLength = DEFAULT_KEY_LENGTH;
    private String keyStr;
    private PlaintextFragment.PlaintextListener plaintextListener;
    private FragmentChangeListener fragmentChangeListener;
    private int[] plaintext = new int[Rijndael.AES_BLOCK_SIZE];
    private int[] block = new int[Rijndael.AES_BLOCK_SIZE];
    private int[] hiddenState;
    private int[] expandedKey;
    private int[] key;
    private boolean mustEncrypt;
    private EncryptTask encryptTask;
    private Context mContext;

    // Perform CPU intensive stuff in another thread instead of on the UI thread

    private static class EncryptTask extends AsyncTask<Void, Boolean, Boolean>
    {
        WeakReference<VisualizationFragment> fragmentWeakReference;

        EncryptTask(VisualizationFragment fragment)
        {
            fragmentWeakReference = new WeakReference<>(fragment);
        }

        @Override
        public Boolean doInBackground(Void... params)
        {
            VisualizationFragment fragment = fragmentWeakReference.get();
            return fragment != null && fragment.doEncryption();
        }

        @Override
        public void onPreExecute()
        {
            VisualizationFragment fragment = fragmentWeakReference.get();

            if (fragment != null)
            {
                View v = fragment.getView();

                if (v != null)
                    v.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onPostExecute(Boolean success)
        {
            VisualizationFragment fragment = fragmentWeakReference.get();

            if (fragment != null)
            {
                View v = fragment.getView();

                if (v != null)
                    v.findViewById(R.id.progressBar).setVisibility(View.GONE);

                if (success)
                    fragment.updateMatrices();
            }
        }
    }

    public VisualizationFragment()
    {
        // Required empty public constructor
    }

    @NonNull
    static VisualizationFragment newInstance()
    {
        return new VisualizationFragment();
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);

        mContext = context;

        if(context instanceof FragmentChangeListener)
            fragmentChangeListener = (FragmentChangeListener)mContext;

        if(context instanceof PlaintextFragment.PlaintextListener)
            plaintextListener = (PlaintextFragment.PlaintextListener)mContext;
    }

    @Override
    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);

        if (bundle == null)
        {
            SharedPreferences preferences = PreferenceManager
                    .getDefaultSharedPreferences(mContext);

            String s = preferences.getString(getString(R.string.prefKeyLength),
                                             "128");

            if(s == null)
                keyLength = DEFAULT_KEY_LENGTH;
            else
                keyLength = Integer.parseInt(s);
        }
        else
        {
            hiddenState = bundle.getIntArray(HIDDEN_STATE);
            expandedKey = bundle.getIntArray(EXPANDED_KEY);
            keyLength = bundle.getInt(KEY_LEN, DEFAULT_KEY_LENGTH);
            keyStr = bundle.getString(KEY);
        }
    }

    @Override
    public void onActivityCreated(Bundle bundle) throws ClassCastException
    {
        super.onActivityCreated(bundle);

        formatKeyStr(false);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater menuInflater)
    {
        super.onCreateOptionsMenu(menu, menuInflater);

        menu.findItem(R.id.menu_submit).setVisible(false);
        menu.findItem(R.id.menu_encrypt).setVisible(true);
        menu.findItem(R.id.menu_settings).setVisible(true);
        menu.findItem(R.id.menu_sbox).setVisible(true);
        menu.findItem(R.id.menu_about).setVisible(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_visualization, container, false);
        final TextView plaintextView = v.findViewById(R.id.plaintextView);
        final EditText keyEditText = v.findViewById(R.id.key_edittext);

        plaintextView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mustEncrypt = true;
                Activity activity = getActivity();

                if(activity != null)
                    ((VisualizationActivity) activity).goToPlaintextEdit();
            }
        });

        plaintextView.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View view, boolean hasFocus)
            {
                plaintextView.setTextColor(
                        ResourcesCompat.getColor(getResources(),
                                                 hasFocus ? R.color.colorAccent :
                                                         android.R.color.primary_text_dark, null));
                plaintextView.setHintTextColor(
                        ResourcesCompat.getColor(getResources(),
                                                 hasFocus ? R.color.colorAccent :
                                                         android.R.color.primary_text_dark, null));
            }
        });

        keyEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent)
            {
                if(i == EditorInfo.IME_ACTION_DONE || i == EditorInfo.IME_NULL)
                {
                    checkAndEncrypt();
                    return true;
                }
                else
                    return false;
            }
        });

        final int maxRounds = Rijndael.getRounds(keyLength);

        GridLayout gridLayout = v.findViewById(R.id.aesGrid);
        gridLayout.setRowCount(maxRounds + 1 + 2);

        return v;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        int length;

        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(mContext);

        String s = preferences.getString(getString(R.string.prefKeyLength), Integer.toString(DEFAULT_KEY_LENGTH));

        if(s == null)
            length = DEFAULT_KEY_LENGTH;
        else
            length = Integer.parseInt(s);

        if (keyLength != length)
        {
            keyLength = length;
            mustEncrypt = true;
            formatKeyStr(true);
            setEncryptionMatrixVisible(false);
        }

        String plaintextStr = plaintextListener.getPlaintext();

        if (plaintextStr != null)
        {
            for (int i = 0; i < plaintext.length; i++)
                plaintext[i] = Integer.parseInt(plaintextStr.substring(2 * i, 2 * (i + 1)), 16);
        }

        if (getView() != null && plaintextListener != null && plaintextListener
                                                                      .getPlaintext() != null)
        {
            StringBuffer strBuf = new StringBuffer(plaintextListener.getPlaintext());

            for (int i = 2; i < 3 * PlaintextFragment.PLAINTEXT_BYTES - 1; i += 3)
                strBuf.insert(i, ' ');

            ((TextView) getView().findViewById(R.id.plaintextView)).setText(strBuf);
            mustEncrypt = true;
            updateMatrices();
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        fragmentChangeListener = null;
        plaintextListener = null;
        mContext = null;
    }

    @Override
    public void onDestroyView()
    {
        View v = getView();

        if(v != null)
        {
            final TextView plaintextView = v.findViewById(R.id.plaintextView);

            // Remove listeners to prevent memory leaks

            plaintextView.setOnClickListener(null);
            plaintextView.setOnFocusChangeListener(null);

            if(encryptTask != null && encryptTask.getStatus() != EncryptTask.Status.FINISHED)
                encryptTask.cancel(true);
        }

        super.onDestroyView();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putString(KEY, keyStr);
        outState.putIntArray(HIDDEN_STATE, hiddenState);
        outState.putIntArray(EXPANDED_KEY, expandedKey);
        outState.putInt(KEY_LEN, keyLength);
    }

    private boolean doEncryption()
    {
        String plaintextStr = plaintextListener.getPlaintext();
        int maxRounds = Rijndael.getRounds(keyLength);

        if(keyStr == null || plaintextStr == null
           || 4*keyStr.length() != keyLength || plaintextStr.length() != 2*Rijndael.AES_BLOCK_SIZE)
        {
            return false;
        }

        if(key == null || key.length < keyLength / 8)
            key = new int[keyLength / 8];

        if(expandedKey == null || expandedKey.length < (1 + maxRounds) * Rijndael.AES_BLOCK_SIZE)
            expandedKey = new int[(1 + maxRounds) * Rijndael.AES_BLOCK_SIZE];

        if(hiddenState == null || hiddenState.length < (1+maxRounds*4)*Rijndael.AES_BLOCK_SIZE)
            hiddenState = new int[(1+maxRounds*4)*Rijndael.AES_BLOCK_SIZE];

        for(int i=0; i < keyLength / 8; i++)
            key[i] = Integer.parseInt(keyStr.substring(2*i,2*(i+1)), 16);

        for(int i=0; i < plaintext.length; i++)
            plaintext[i] = Integer.parseInt(plaintextStr.substring(2*i, 2*(i+1)), 16);

        Rijndael.expandKey(mContext, expandedKey, key, keyLength);

        System.arraycopy(plaintext, 0, block, 0, Rijndael.AES_BLOCK_SIZE);
        System.arraycopy(block, 0, hiddenState, 0, Rijndael.AES_BLOCK_SIZE);

        for (int round = 0; round <= maxRounds; round++)
        {
            if(round > 0)
            {
                block = Rijndael.subBytes(mContext, block, block);
                System.arraycopy(block, 0, hiddenState, (round*4-2)*Rijndael.AES_BLOCK_SIZE,
                                 Rijndael.AES_BLOCK_SIZE);
                block = Rijndael.shiftRows(block, block);
                System.arraycopy(block, 0, hiddenState, (round*4-1)*Rijndael.AES_BLOCK_SIZE,
                                 Rijndael.AES_BLOCK_SIZE);

                if(round < maxRounds)
                {
                    block = Rijndael
                            .mixColumns(mContext, block, round, maxRounds, block);
                    System.arraycopy(block, 0, hiddenState, (round * 4) * Rijndael.AES_BLOCK_SIZE,
                                     Rijndael.AES_BLOCK_SIZE);
                }
            }

            block = Rijndael.addRoundKey(block, round, expandedKey, block);
            System.arraycopy(block, 0, hiddenState,
                             Rijndael.AES_BLOCK_SIZE*(1+round*4-(round < maxRounds ? 0 : 1)),
                             Rijndael.AES_BLOCK_SIZE);
        }

        mustEncrypt = false;

        return true;
    }

    private void updateMatrices()
    {
        final View v = getView();

        if(v == null)
            return;

        if(mustEncrypt)
        {
            encryptTask = new EncryptTask(this);
            encryptTask.execute();
            return;
        }

        final int maxRounds = Rijndael.getRounds(keyLength);
        TextView ciphertextView = v.findViewById(R.id.ciphertext_textview);
        GridLayout gridLayout = v.findViewById(R.id.aesGrid);

        gridLayout.removeAllViews();

        AesMatrixView plaintextMatrix = v.findViewById(R.id.plaintextMatrix);
        plaintextMatrix.setValues(plaintext);

        for (int round = 0; round <= maxRounds; round++)
        {
            AesMatrixView matrixView;

            if (round > 0)
            {
                if(round % 2 == 1)
                {
                    final TextView roundView = new TextView(gridLayout.getContext());
                    final TextView subBytesView = new TextView(gridLayout.getContext());
                    final TextView shiftRowsView = new TextView(gridLayout.getContext());
                    final TextView mixColumnsView = new TextView(gridLayout.getContext());
                    final TextView addRoundKeyView = new TextView(gridLayout.getContext());
                    final TextView roundKeyView = new TextView(gridLayout.getContext());

                    roundView.setText(getString(R.string.round));
                    subBytesView.setText(getString(R.string.subBytes));
                    shiftRowsView.setText(getString(R.string.shiftRows));
                    mixColumnsView.setText(getString(R.string.mixColumns));
                    addRoundKeyView.setText(getString(R.string.addRoundKey));
                    roundKeyView.setText(getString(R.string.roundKey));

                    gridLayout.addView(roundView);
                    gridLayout.addView(subBytesView);
                    gridLayout.addView(shiftRowsView);
                    gridLayout.addView(mixColumnsView);
                    gridLayout.addView(addRoundKeyView);
                    gridLayout.addView(roundKeyView);

                    ((GridLayout.LayoutParams) roundView.getLayoutParams()).columnSpec =
                            GridLayout.spec(0, GridLayout.CENTER);
                    ((GridLayout.LayoutParams) roundView.getLayoutParams()).rowSpec =
                            GridLayout.spec(2 + 3 * ((round - 1) / 2), GridLayout.CENTER);
                    ((GridLayout.LayoutParams) subBytesView.getLayoutParams()).columnSpec =
                            GridLayout.spec(1, GridLayout.CENTER);
                    ((GridLayout.LayoutParams) subBytesView.getLayoutParams()).rowSpec =
                            GridLayout.spec(2 + 3 * ((round - 1) / 2), GridLayout.CENTER);
                    ((GridLayout.LayoutParams) shiftRowsView.getLayoutParams()).columnSpec =
                            GridLayout.spec(2, GridLayout.CENTER);
                    ((GridLayout.LayoutParams) shiftRowsView.getLayoutParams()).rowSpec =
                            GridLayout.spec(2 + 3 * ((round - 1) / 2), GridLayout.CENTER);
                    ((GridLayout.LayoutParams) mixColumnsView.getLayoutParams()).columnSpec =
                            GridLayout.spec(3, GridLayout.CENTER);
                    ((GridLayout.LayoutParams) mixColumnsView.getLayoutParams()).rowSpec =
                            GridLayout.spec(2 + 3 * ((round - 1) / 2), GridLayout.CENTER);
                    ((GridLayout.LayoutParams) addRoundKeyView.getLayoutParams()).columnSpec =
                            GridLayout.spec(4, GridLayout.CENTER);
                    ((GridLayout.LayoutParams) addRoundKeyView.getLayoutParams()).rowSpec =
                            GridLayout.spec(2 + 3 * ((round - 1) / 2), GridLayout.CENTER);
                    ((GridLayout.LayoutParams) roundKeyView.getLayoutParams()).columnSpec =
                            GridLayout.spec(5, GridLayout.CENTER);
                    ((GridLayout.LayoutParams) roundKeyView.getLayoutParams()).rowSpec =
                            GridLayout.spec(2 + 3 * ((round - 1) / 2), GridLayout.CENTER);
                }

                TextView currentRoundView = new TextView(gridLayout.getContext());

                currentRoundView.setText(String.format(Locale.ENGLISH, "%d", round));
                gridLayout.addView(currentRoundView);

                ((GridLayout.LayoutParams) currentRoundView.getLayoutParams()).columnSpec =
                        GridLayout.spec(0, GridLayout.CENTER);
                ((GridLayout.LayoutParams) currentRoundView.getLayoutParams()).rowSpec =
                        GridLayout.spec(3*((round+1)/2)+(round-1)%2, GridLayout.CENTER);

                // SubBytes
                matrixView = new AesMatrixView(gridLayout.getContext());

                System.arraycopy(hiddenState, (round*4-2)*Rijndael.AES_BLOCK_SIZE, block, 0,
                                 Rijndael.AES_BLOCK_SIZE);
                matrixView.setValues(block);
                gridLayout.addView(matrixView);

                ((GridLayout.LayoutParams) matrixView.getLayoutParams()).columnSpec =
                        GridLayout.spec(1, GridLayout.CENTER);
                ((GridLayout.LayoutParams) matrixView.getLayoutParams()).rowSpec =
                        GridLayout.spec(3*((round+1)/2)+(round-1)%2, GridLayout.CENTER);

                // ShiftRows

                matrixView = new AesMatrixView(gridLayout.getContext());

                System.arraycopy(hiddenState, (round*4-1)*Rijndael.AES_BLOCK_SIZE, block, 0,
                                 Rijndael.AES_BLOCK_SIZE);
                matrixView.setValues(block);
                gridLayout.addView(matrixView);

                ((GridLayout.LayoutParams) matrixView.getLayoutParams()).columnSpec =
                        GridLayout.spec(2, GridLayout.CENTER);
                ((GridLayout.LayoutParams) matrixView.getLayoutParams()).rowSpec =
                        GridLayout.spec(3*((round+1)/2)+(round-1)%2, GridLayout.CENTER);

                if (round < maxRounds)
                {
                    // MixColumns

                    matrixView = new AesMatrixView(gridLayout.getContext());

                    System.arraycopy(hiddenState, (round*4)*Rijndael.AES_BLOCK_SIZE, block, 0,
                                     Rijndael.AES_BLOCK_SIZE);

                    matrixView.setValues(block);
                    gridLayout.addView(matrixView);

                    ((GridLayout.LayoutParams) matrixView.getLayoutParams()).columnSpec =
                            GridLayout.spec(3, GridLayout.CENTER);
                    ((GridLayout.LayoutParams) matrixView.getLayoutParams()).rowSpec =
                            GridLayout.spec(3*((round+1)/2)+(round-1)%2, GridLayout.CENTER);
                }
            }

            // AddRoundKey

            if(round == 0)
                matrixView = v.findViewById(R.id.addRoundKeyMatrix);
            else
                matrixView = new AesMatrixView(gridLayout.getContext());

            System.arraycopy(hiddenState,
                             Rijndael.AES_BLOCK_SIZE*(1+round*4-(round < maxRounds ? 0 : 1)),
                             block, 0, Rijndael.AES_BLOCK_SIZE);
            matrixView.setValues(block);

            if (round > 0)
            {
                gridLayout.addView(matrixView);

                ((GridLayout.LayoutParams) matrixView.getLayoutParams()).columnSpec =
                        GridLayout.spec(4, GridLayout.CENTER);
                ((GridLayout.LayoutParams) matrixView.getLayoutParams()).rowSpec =
                        GridLayout.spec(3*((round+1)/2)+(round-1)%2, GridLayout.CENTER);
            }

            // Display the round key

            if(round == 0)
                matrixView = v.findViewById(R.id.roundKeyMatrix);
            else
                matrixView = new AesMatrixView(gridLayout.getContext());

            System.arraycopy(expandedKey, round * Rijndael.AES_BLOCK_SIZE, block, 0,
                             Rijndael.AES_BLOCK_SIZE);
            matrixView.setValues(block);

            if(round > 0)
            {
                gridLayout.addView(matrixView);

                ((GridLayout.LayoutParams) matrixView.getLayoutParams()).columnSpec =
                        GridLayout.spec(5, GridLayout.CENTER);
                ((GridLayout.LayoutParams) matrixView.getLayoutParams()).rowSpec =
                        GridLayout.spec(3*((round+1)/2)+(round-1)%2, GridLayout.CENTER);
            }
        }

        System.arraycopy(hiddenState, Rijndael.AES_BLOCK_SIZE*(maxRounds*4), block, 0,
                         Rijndael.AES_BLOCK_SIZE);

        StringBuilder sb = new StringBuilder(3 * (keyLength / 8) - 1);

        for (int i = 0; i < Rijndael.AES_BLOCK_SIZE; i++)
        {
            sb.append(String.format(Locale.ENGLISH,
                                    "%02X" + ((i + 1 == Rijndael.AES_BLOCK_SIZE) ? "" : " "),
                                    block[i]));
        }

        ciphertextView.setText(sb.toString());

        // Show the ciphertext and block matrices

        setEncryptionMatrixVisible(true);
    }

    private boolean formatKeyStr(boolean zero)
    {
        StringBuilder keyStringBuilder;
        String keyString;
        Pattern keyPattern = Pattern.compile("[A-Fa-f0-9]*");

        if (getView() == null)
            return false;

        EditText keyEditText = getView().findViewById(R.id.key_edittext);
        keyString = keyEditText.getText().toString().replaceAll(" ", "");

        // Adjust the max length of the editText

        InputFilter[] filters = keyEditText.getFilters();

        for (int i = 0; i < filters.length; i++)
        {
            if (filters[i] instanceof InputFilter.LengthFilter)
            {
                filters[i] = new InputFilter.LengthFilter(keyLength / 4);
                break;
            }
        }

        keyEditText.setFilters(filters);

        if(keyPattern.matcher(keyString).matches())
        {
            keyStringBuilder = new StringBuilder(keyString);

            // If the key in the EditText is too short then extend it by padding zeros

            if (zero)
            {
                for (int i = keyStringBuilder.length(); i < keyLength / 4; i++)
                    keyStringBuilder.append('0');
            }

            keyStr = keyStringBuilder.toString().toUpperCase();
            keyEditText.setText(keyStr);

            return true;
        }
        else
            return false;
    }

    private void setEncryptionMatrixVisible(boolean visible)
    {
        View root = getView();

        if(root != null)
        {
            root.findViewById(R.id.ciphertextLayout)
                    .setVisibility(visible ? View.VISIBLE : View.GONE);
            root.findViewById(R.id.gridScrollView)
                    .setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }

    private void checkAndEncrypt()
    {
        View root = getView();

        if(root != null)
        {
            // Is the key valid for its key length? If too short, pad zeros. If too long,
            // show error message

            setEncryptionMatrixVisible(false);

            if(plaintextListener.getPlaintext() == null)
            {
                Toast.makeText(getActivity(), R.string.plaintextMissing, Toast.LENGTH_SHORT)
                        .show();
            }
            else if(formatKeyStr(true))
            {
                mustEncrypt = true;
                updateMatrices();
                fragmentChangeListener.hideKeyboard();
            }
            else
            {
                Toast.makeText(getActivity(), R.string.keyInvalid, Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.menu_encrypt:
                if(fragmentChangeListener != null)
                    fragmentChangeListener.hideKeyboard();

                checkAndEncrypt();
                return true;
            case R.id.menu_about:
                if(fragmentChangeListener != null)
                    fragmentChangeListener.goToAbout();
                return true;
            case R.id.menu_sbox:
                if(fragmentChangeListener != null)
                    fragmentChangeListener.goToSbox();
                return true;
            case R.id.menu_settings:
                if(fragmentChangeListener != null)
                    fragmentChangeListener.goToSettings();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
