package me.tiju.aes;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;

import com.google.firebase.analytics.FirebaseAnalytics;

import androidx.preference.CheckBoxPreference;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

public class SettingsFragment extends PreferenceFragmentCompat
{
    private Context mContext;

    public SettingsFragment()
    {

    }

    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);

        mContext = context;
    }

    @NonNull
    static SettingsFragment newInstance()
    {
        return new SettingsFragment();
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
    {
        addPreferencesFromResource(R.xml.preferences);

        final ListPreference keySizePreference = getPreferenceManager()
                .findPreference(getString(R.string.prefKeyLength));
        final CheckBoxPreference analyticsConsentPreference = getPreferenceManager()
                .findPreference(getString(R.string.pref_analytics_consent));

        if(keySizePreference != null)
        {
            Preference.OnPreferenceChangeListener onPreferenceChangeListener =
                    new Preference.OnPreferenceChangeListener()
            {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue)
                {
                    int index = getIndexOfValue((String) newValue);

                    if (index != -1)
                    {
                        keySizePreference.setSummary(
                                getResources().getStringArray(R.array.key_size_entries)[index]);
                    }
                    else
                        keySizePreference.setSummary("");

                    return true;
                }
            };

            int index = getIndexOfValue(PreferenceManager
                                                .getDefaultSharedPreferences(mContext)
                                                .getString(keySizePreference.getKey(),
                                                           "128"));

            if(index != -1)
            {
                keySizePreference.setSummary(
                        getResources().getStringArray(R.array.key_size_entries)[index]);
            }
            else
                keySizePreference.setSummary("");

            keySizePreference.setOnPreferenceChangeListener(onPreferenceChangeListener);
        }

        if(analyticsConsentPreference != null)
        {
            Preference.OnPreferenceChangeListener onAnalyticsConsentPreferenceChangeListener =
                    new Preference.OnPreferenceChangeListener()
            {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue)
                {
                    FirebaseAnalytics analytics = FirebaseAnalytics.getInstance(preference.getContext());
                    boolean hasAnalyticsConsent = (boolean)newValue;

                    analytics.setAnalyticsCollectionEnabled(hasAnalyticsConsent);

                    if(!hasAnalyticsConsent)
                        analytics.resetAnalyticsData();

                    return true;
                }
            };

            analyticsConsentPreference.setOnPreferenceChangeListener(onAnalyticsConsentPreferenceChangeListener);
        }
    }

    @Override
    public void onDisplayPreferenceDialog(Preference preference)
    {
        super.onDisplayPreferenceDialog(preference);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        final ListPreference keySizePreference = getPreferenceManager()
                .findPreference(getString(R.string.prefKeyLength));
        final CheckBoxPreference adConsentPreference = getPreferenceManager()
                .findPreference(getString(R.string.pref_ad_consent));
        final CheckBoxPreference analyticsConsentPreference = getPreferenceManager()
                .findPreference(getString(R.string.pref_analytics_consent));

        if(keySizePreference != null)
            keySizePreference.setOnPreferenceChangeListener(null);

        if(adConsentPreference != null)
            adConsentPreference.setOnPreferenceChangeListener(null);

        if(analyticsConsentPreference != null)
            analyticsConsentPreference.setOnPreferenceChangeListener(null);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        mContext = null;
    }

    @CheckResult
    private int getIndexOfValue(String value)
    {
        if(value == null)
            return -1;

        String[] entries = getResources().getStringArray(R.array.key_size_entry_values);

        for(int index=0; index < entries.length; index++)
        {
            if(entries[index].equalsIgnoreCase(value))
                return index;
        }

        return -1;
    }
}
