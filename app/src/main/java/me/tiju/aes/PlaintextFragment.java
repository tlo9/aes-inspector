package me.tiju.aes;

import android.app.Activity;
import androidx.fragment.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

public class PlaintextFragment extends DialogFragment
{
    final private static int FILE_DIALOG_REQUEST = 1234;
    static int PLAINTEXT_BYTES = 16;
    private static String PLAINTEXT = "plaintext";
    private static String FILENAME = "filename";
    private static String TEXT_STR = "textStr";
    private static String HEX_STR = "hexStr";
    private static String FILE_OFFSET = "fileOffset";
    private static String LOAD_OPTION = "inputType";
    private TextWatcher textWatcher;
    private Context mContext;

    private String plaintext;
    private Uri filenameUri;
    private PlaintextListener plaintextListener;
    private FragmentChangeListener fragmentChangeListener;
    private DismissListener dismissListener;

    public PlaintextFragment()
    {
        // Required empty public constructor
    }

    interface PlaintextListener
    {
        void setPlaintext(String plaintext);

        String getPlaintext();
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        mContext = context;

        if (context instanceof FragmentChangeListener)
            fragmentChangeListener = (FragmentChangeListener) context;

        if (context instanceof DismissListener)
            dismissListener = (DismissListener) context;

        if (context instanceof PlaintextListener)
            plaintextListener = (PlaintextListener) context;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();

        mContext = null;
        fragmentChangeListener = null;
        plaintextListener = null;
        dismissListener = null;
    }

    @NonNull
    static PlaintextFragment newInstance(String inPlaintext)
    {
        PlaintextFragment plaintextFragment = new PlaintextFragment();

        Bundle args = new Bundle();
        args.putString(PLAINTEXT, inPlaintext);

        plaintextFragment.setArguments(args);
        return plaintextFragment;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);

        menu.findItem(R.id.menu_submit).setVisible(true);
        menu.findItem(R.id.menu_encrypt).setVisible(false);
        menu.findItem(R.id.menu_settings).setVisible(false);
        menu.findItem(R.id.menu_sbox).setVisible(false);
        menu.findItem(R.id.menu_about).setVisible(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null)
        {
            plaintext = savedInstanceState.getString(PLAINTEXT);

            if (savedInstanceState.getString(FILENAME, null) != null)
                filenameUri = Uri.parse(savedInstanceState.getString(FILENAME));
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_plaintext, container, false);

        Button loadButton = view.findViewById(R.id.loadButton);
        RadioButton textRadioButton = view.findViewById(R.id.textRadioButton),
                fileRadioButton = view.findViewById(R.id.fileRadioButton),
                hexRadioButton = view.findViewById(R.id.hexRadioButton);
        final LinearLayout textLayout = view.findViewById(R.id.textLayout);
        final LinearLayout fileLayout = view.findViewById(R.id.fileLayout);
        final LinearLayout hexLayout = view.findViewById(R.id.hexLayout);

        textRadioButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                textLayout.setVisibility(View.VISIBLE);
                fileLayout.setVisibility(View.GONE);
                hexLayout.setVisibility(View.GONE);
            }
        });

        fileRadioButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                fileLayout.setVisibility(View.VISIBLE);
                textLayout.setVisibility(View.GONE);
                hexLayout.setVisibility(View.GONE);
            }
        });

        hexRadioButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                fileLayout.setVisibility(View.GONE);
                textLayout.setVisibility(View.GONE);
                hexLayout.setVisibility(View.VISIBLE);
            }
        });

        loadButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent()
                        .setType("*/*")
                        .addCategory(Intent.CATEGORY_OPENABLE)
                        .setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent, "Select a file"),
                                       FILE_DIALOG_REQUEST);
            }
        });

        final EditText hexEdit = view.findViewById(R.id.hexPlaintextData);
        final EditText textEdit = view.findViewById(R.id.textPlaintextData);
        final EditText offsetEdit = view.findViewById(R.id.fileOffset);
        final TextView plaintextView = view.findViewById(R.id.plaintext);
        final View plaintextLayout = view.findViewById(R.id.plaintextLayout);

        TextView.OnEditorActionListener onEditorActionListener = new EditText.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent)
            {
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_NULL)
                {
                    loadAndShowPlaintext();

                    if (fragmentChangeListener != null)
                        fragmentChangeListener.hideKeyboard();
                    return true;
                }
                else
                    return false;
            }
        };

        textWatcher = new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
                loadAndShowPlaintext();
            }

            @Override
            public void afterTextChanged(Editable editable)
            {

            }
        };

        hexEdit.addTextChangedListener(textWatcher);
        textEdit.addTextChangedListener(textWatcher);
        offsetEdit.addTextChangedListener(textWatcher);

        hexEdit.setOnEditorActionListener(onEditorActionListener);
        textEdit.setOnEditorActionListener(onEditorActionListener);
        offsetEdit.setOnEditorActionListener(onEditorActionListener);

        if (savedInstanceState != null)
        {
            int loadOption = savedInstanceState.getInt(LOAD_OPTION, -1);

            if (loadOption != -1)
            {
                ((RadioButton) view.findViewById(loadOption)).setChecked(true);
                (view.findViewById(loadOption)).performClick();
            }

            hexEdit.setText(savedInstanceState.getString(HEX_STR));
            offsetEdit.setText(savedInstanceState.getString(FILE_OFFSET));
            textEdit.setText(savedInstanceState.getString(TEXT_STR));
        }

        if (plaintext == null && getArguments() != null)
            plaintext = getArguments().getString(FILENAME);
        else
        {
            plaintextView.setText(formatPlaintext(plaintext));
            plaintextLayout.setVisibility(View.VISIBLE);
        }

        return view;
    }

    private void loadAndShowPlaintext()
    {
        final View view = getView();

        if (view == null)
            return;

        final TextView plaintextView = view.findViewById(R.id.plaintext);
        final View plaintextLayout = view.findViewById(R.id.plaintextLayout);
        final EditText offsetLayout = view.findViewById(R.id.fileOffset);

        if (loadPlaintext())
        {
            plaintextView.setText(formatPlaintext(plaintext));
            plaintextLayout.setVisibility(View.VISIBLE);

            if (filenameUri != null)
            {
                String result = null;
                String scheme = filenameUri.getScheme();

                if (scheme != null && scheme.equals("content"))
                {
                    Cursor cursor = mContext
                            .getContentResolver()
                            .query(filenameUri, null, null, null,
                                   null);
                    try
                    {
                        if (cursor != null && cursor.moveToFirst())
                        {
                            result = cursor
                                    .getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                        }
                    }
                    finally
                    {
                        if (cursor != null)
                            cursor.close();
                    }
                }

                if (result != null)
                {
                    ((TextView) view.findViewById(R.id.filename))
                            .setText(result);
                    offsetLayout.setVisibility(View.VISIBLE);
                }
                else
                {
                    ((TextView) view.findViewById(R.id.filename)).setText("");
                    offsetLayout.setVisibility(View.GONE);
                }
            }
            else
            {
                ((TextView) view.findViewById(R.id.filename)).setText("");
                offsetLayout.setVisibility(View.GONE);
            }
        }
        else
        {
            plaintextLayout.setVisibility(View.INVISIBLE);
            offsetLayout.setVisibility(View.GONE);
        }
    }

    private String formatPlaintext(String inStr)
    {
        StringBuilder sb = new StringBuilder(inStr);

        for (int i = 0; i < 15; i++)
            sb.insert(2 + 3 * i, ' ');

        return sb.toString().toUpperCase();
    }

    @Nullable
    private String loadFileData()
    {
        byte[] data = new byte[PLAINTEXT_BYTES];
        int offset;

        if (filenameUri != null && getView() != null)
        {
            final EditText offsetView = getView().findViewById(R.id.fileOffset);

            try
            {
                offset = Integer.parseInt(offsetView.getText().toString());
            }
            catch (NumberFormatException e)
            {
                if (offsetView.getText().toString().trim().equals(""))
                    offset = 0;
                else
                    return null;
            }

            if (offset < 0)
            {
                Toast.makeText(mContext, R.string.offsetInvalid, Toast.LENGTH_SHORT).show();
                return null;
            }

            try
            {
                InputStream inputStream = mContext.getContentResolver()
                        .openInputStream(filenameUri);

                if (inputStream != null)
                {
                    inputStream.skip(offset);

                    int bytesRead = inputStream.read(data, 0, PLAINTEXT_BYTES);
                    inputStream.close();

                    StringBuilder sb = new StringBuilder(2 * Rijndael.AES_BLOCK_SIZE);

                    if (bytesRead == -1)
                        bytesRead = 0;

                    for (int i = 0; i < bytesRead; i++)
                        sb.append(
                                String.format(Locale.getDefault(), "%02X", ((int) data[i]) & 0xFF));

                    for (int i = bytesRead; i < Rijndael.AES_BLOCK_SIZE; i++)
                        sb.append("00");

                    return sb.toString();
                }
                else
                    return null;
            }
            catch (FileNotFoundException e)
            {
                Toast.makeText(mContext, R.string.fileNotFound, Toast.LENGTH_LONG)
                        .show();
                return null;
            }
            catch (IOException | NullPointerException e)
            {
                Toast.makeText(mContext, R.string.loadError, Toast.LENGTH_LONG)
                        .show();
                return null;
            }
        }
        return null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == FILE_DIALOG_REQUEST)
        {
            if (resultCode == Activity.RESULT_OK)
            {
                filenameUri = data.getData();
                loadAndShowPlaintext();
            }
        }
        else
            super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        if (item.getItemId() == R.id.menu_submit)
        {
            if (plaintextListener == null || !loadPlaintext())
                return true;

            plaintextListener.setPlaintext(plaintext);

            if (plaintext != null)
            {
                dismiss();

                if (dismissListener != null)
                    dismissListener.onDismiss();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean loadPlaintext()
    {
        int multiplier = 1;
        View root = getView();
        RadioGroup radioGroup;

        if (root == null)
            return false;

        plaintext = null;
        radioGroup = root.findViewById(R.id.loadRadioGroup);
        StringBuilder stringBuilder = new StringBuilder(2 * PLAINTEXT_BYTES);

        switch (radioGroup.getCheckedRadioButtonId())
        {
            case R.id.hexRadioButton:
                multiplier = 2;
            case R.id.textRadioButton:
                if (multiplier == 1)
                {
                    plaintext = ((EditText) root.findViewById(R.id.textPlaintextData))
                            .getText().toString();
                }
                else
                {
                    plaintext = ((EditText) root.findViewById(R.id.hexPlaintextData))
                            .getText().toString();
                }
                if (multiplier == 1)
                {
                    for (byte b : plaintext.getBytes())
                    {
                        if (stringBuilder.length() >= 2 * PLAINTEXT_BYTES)
                            break;

                        stringBuilder.append(String.format(Locale.getDefault(), "%02X",
                                                           ((int) b) & 0xff).toUpperCase());
                    }
                }
                else
                    stringBuilder.append(plaintext);

                for (int i = stringBuilder.length(); i < 2 * PLAINTEXT_BYTES; i++)
                    stringBuilder.append('0');

                plaintext = stringBuilder.substring(0, 2 * PLAINTEXT_BYTES);
                break;
            case R.id.fileRadioButton:
                plaintext = loadFileData();
                break;
            default:
                break;
        }

        return plaintext != null;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedState)
    {
        super.onSaveInstanceState(savedState);
        View v = getView();

        if (v != null)
        {
            RadioGroup radioGroup = v.findViewById(R.id.loadRadioGroup);

            savedState.putInt(LOAD_OPTION, radioGroup.getCheckedRadioButtonId());
            savedState.putString(TEXT_STR, ((EditText) v.findViewById(R.id.textPlaintextData))
                    .getText().toString());
            savedState.putString(FILE_OFFSET, ((EditText) v.findViewById(R.id.fileOffset))
                    .getText().toString());
            savedState.putString(HEX_STR, ((EditText) v.findViewById(R.id.hexPlaintextData))
                    .getText().toString());

            if (filenameUri != null)
                savedState.putString(FILENAME, filenameUri.toString());
            savedState.putString(PLAINTEXT, plaintext);
        }
    }

    @Override
    public void onDestroyView()
    {
        if (getView() != null)
        {
            getView().findViewById(R.id.loadButton).setOnClickListener(null);
            getView().findViewById(R.id.textRadioButton).setOnClickListener(null);
            getView().findViewById(R.id.fileRadioButton).setOnClickListener(null);

            ((EditText) getView().findViewById(R.id.textPlaintextData))
                    .setOnEditorActionListener(null);
            ((EditText) getView().findViewById(R.id.hexPlaintextData))
                    .setOnEditorActionListener(null);
            ((EditText) getView().findViewById(R.id.fileOffset))
                    .setOnEditorActionListener(null);
            ((EditText) getView().findViewById(R.id.textPlaintextData))
                    .removeTextChangedListener(textWatcher);
            ((EditText) getView().findViewById(R.id.hexPlaintextData))
                    .removeTextChangedListener(textWatcher);
            ((EditText) getView().findViewById(R.id.fileOffset))
                    .removeTextChangedListener(textWatcher);
        }

        textWatcher = null;

        super.onDestroyView();
    }
}