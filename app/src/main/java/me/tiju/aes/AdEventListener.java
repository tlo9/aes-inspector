package me.tiju.aes;

public interface AdEventListener
{
    void onAdOpened(int adId);
    void onAdClosed(int adId);
    void onAdLoaded(int adId);
    void onAdError(int adId, int code);
    void onAdClicked(int adId);

    // Notify the AdEventListener to call loadAd() on the AdManager after a certain amount of time
    void onAdScheduleReload(int adId, long delay);
}
