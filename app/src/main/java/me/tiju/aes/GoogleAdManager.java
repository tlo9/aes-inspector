package me.tiju.aes;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;

import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import java.util.Calendar;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class GoogleAdManager extends AdManager
{
    @Nullable
    private AdEventListener mAdEventListener;
    @Nullable
    private AdRequest mAdRequest;
    private static final String TEST_INTERSTITIAL_UNIT_ID = "ca-app-pub-3940256099942544/8691691433";
    private static final String TEST_BANNER_UNIT_ID = "ca-app-pub-3940256099942544/6300978111";
    private static long mLastShown; // Time interstitial was last shown
    private boolean mHasConsent;
    private AdSize mAdSize;

    GoogleAdManager(@NonNull Context context, @Nullable AdEventListener adEventListener)
    {
        super(context);

        mAdEventListener = adEventListener;
        mAdSize = AdSize.SMART_BANNER;
    }

    private void buildAdRequest(@NonNull Context context)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        mHasConsent = preferences.getBoolean(context.getString(R.string.pref_ad_consent), true);

        // Initialize an ad request and load a banner ad

        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        if (BuildConfig.DEBUG || BuildConfig.FLAVOR
                .equalsIgnoreCase(context.getString(R.string.test_flavor)))
        {
            adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        }

        String[] testDeviceIds = context.getResources().getStringArray(R.array.test_device_ids);

        for (String testDeviceId : testDeviceIds)
            adRequestBuilder.addTestDevice(testDeviceId);

        if (mHasConsent)
            adRequestBuilder.tagForChildDirectedTreatment(false);
        else
        {
            Bundle bundle = new Bundle();
            bundle.putString("npa", "1");

            adRequestBuilder.addNetworkExtrasBundle(AdMobAdapter.class, bundle);
        }

        mAdRequest = adRequestBuilder.build();
    }

    @Override
    public void setConsent(boolean grantConsent)
    {
        super.setConsent(grantConsent);

        if(grantConsent != mHasConsent)
        {
            mHasConsent = grantConsent;
            Context context = getContext();

            if(context != null)
            {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

                mHasConsent = preferences
                        .getBoolean(context.getString(R.string.pref_ad_consent), true);

                onCreate(context);
            }
        }
    }

    // Initialize ad by sending ad requests to the ad servers
    // Assumes that ads have been already created

    @Override
    public void onCreate(Context context)
    {
        if (!isAdsEnabled())
        {
            return;
        }

        buildAdRequest(context);
    }

    @Override
    public void onResume(Context context)
    {
        for(Integer id : mAdIds)
        {
            Object obj = getAdObject(id);

            if(obj != null && getType(id) == AdType.BANNER)
                ((AdView)obj).resume();
        }
    }

    @Override
    public void onPause(Context context)
    {
        for(Integer id : mAdIds)
        {
            Object obj = getAdObject(id);

            if(obj != null && getType(id) == AdType.BANNER)
                ((AdView)obj).pause();
        }
    }

    @Override
    public void onDestroy(Context context)
    {
        mAdEventListener = null;

        for(Integer id : mAdIds)
            destroyAd(id, false);

        mAdIds.clear();

        super.onDestroy(context);
    }

    @Override
    public void onStart(Context context)
    {
        for(int id : mAdIds)
            loadAd(id);
    }

    @Override
    public void loadAd(int id)
    {
        if(id == INVALID_ID || mAdRequest == null)
            return;

        Object adObject = getAdObject(id);

        if(adObject != null)
        {
            switch (getType(id))
            {
                case AdType.BANNER:
                    ((AdView) adObject).loadAd(mAdRequest);
                    break;
                case AdType.INTERSTITIAL:
                    ((InterstitialAd) adObject).loadAd(mAdRequest);
                    break;
                case AdType.INVALID:
                default:
                    break;
            }
        }
    }

    @Override
    public void onStop(Context context)
    {

    }

    // This should be done before onCreate()

    @Override
    public int createAd(int type)
    {
        final Context context = getContext();

        if(context == null)
            return AdManager.INVALID_ID;

        final int id = super.createAd(type);

        if(id == AdManager.INVALID_ID)
            return AdManager.INVALID_ID;

        if(type == AdType.BANNER)
        {
            final AdView adView = new AdView(context);

            setAdObject(id, adView);

            adView.setAdSize(mAdSize);

            if (BuildConfig.DEBUG || BuildConfig.FLAVOR
                    .equalsIgnoreCase(getContext().getString(R.string.test_flavor)))
                adView.setAdUnitId(TEST_BANNER_UNIT_ID);
            else
                adView.setAdUnitId(getContext().getString(R.string.admob_banner_id));

            adView.setId(R.id.adView);

            AdListener bannerAdListener = new AdListener()
            {
                @Override
                public void onAdOpened()
                {
                    super.onAdOpened();

                    if(mAdEventListener != null)
                        mAdEventListener.onAdOpened(id);
                }

                @Override
                public void onAdLoaded()
                {
                    super.onAdLoaded();

                    if(mAdEventListener != null)
                        mAdEventListener.onAdLoaded(id);
                }

                @Override
                public void onAdFailedToLoad(int code)
                {
                    super.onAdFailedToLoad(code);

                    if(mAdEventListener != null)
                        mAdEventListener.onAdError(id, code);
                }
            };

            adView.setAdListener(bannerAdListener);
        }
        else if(type == AdType.INTERSTITIAL)
        {
            final InterstitialAd interstitialAd = new InterstitialAd(
                    context.getApplicationContext());

            setAdObject(id, interstitialAd);

            // Initialize an interstitial ad

            if (BuildConfig.DEBUG || BuildConfig.FLAVOR
                    .equalsIgnoreCase(getContext().getString(R.string.test_flavor)))
                interstitialAd.setAdUnitId(TEST_INTERSTITIAL_UNIT_ID);
            else
                interstitialAd.setAdUnitId(getContext().getString(R.string.admob_interstitial_id));

            AdListener interstitialAdListener = new AdListener()
            {
                @Override
                public void onAdLeftApplication()
                {
                    super.onAdLeftApplication();

                    if(mAdEventListener != null)
                        mAdEventListener.onAdClicked(id);
                }

                @Override
                public void onAdOpened()
                {
                    super.onAdOpened();

                    if(mAdEventListener != null)
                        mAdEventListener.onAdOpened(id);
                }

                @Override
                public void onAdLoaded()
                {
                    super.onAdLoaded();

                    if(mAdEventListener != null)
                        mAdEventListener.onAdLoaded(id);
                }

                @Override
                public void onAdFailedToLoad(int code)
                {
                    super.onAdFailedToLoad(code);

                    if(mAdEventListener != null)
                        mAdEventListener.onAdError(id, code);
                }

                @Override
                public void onAdClosed()
                {
                    super.onAdClosed();

                    setLastShown(id);

                    if(mAdEventListener != null)
                        mAdEventListener.onAdClosed(id);

                    if(mAdRequest != null)
                        interstitialAd.loadAd(mAdRequest);
                }
            };

            interstitialAd.setAdListener(interstitialAdListener);
        }

        return id;
    }

    @Override
    public void destroyAd(int id)
    {
        destroyAd(id, true);
    }

    private void destroyAd(int id, boolean removeId)
    {
        if(getType(id) == AdType.BANNER)
        {
            AdView adView = (AdView)getAdObject(id);

            if(adView != null)
            {
                adView.setAdListener(null);
                adView.destroy();
            }
        }
        else if(getType(id) == AdType.INTERSTITIAL)
        {
            InterstitialAd interstitialAd = (InterstitialAd)getAdObject(id);

            if(interstitialAd != null)
                interstitialAd.setAdListener(null);
        }

        if(removeId)
            super.destroyAd(id);
    }

    @Override
    public boolean showAd(int id)
    {
        super.showAd(id);

        if(id == AdManager.INVALID_ID)
            return false;

        Object obj = getAdObject(id);

        if (!isAdsEnabled())
        {
            return false;
        }

        switch(getType(id))
        {
            case AdType.BANNER:
                if (obj != null)
                    ((AdView) obj).setVisibility(View.VISIBLE);
                return true;
            case AdType.INTERSTITIAL:
                return showInterstitialAd(id);
            case AdType.INVALID:
            default:
                return false;
        }
    }

    /* Advertisements */

    // Returns true if an ad was shown

    private boolean showInterstitialAd(int id)
    {
        if(id == AdManager.INVALID_ID)
            return false;

        final InterstitialAd interstitialAd = (InterstitialAd)getAdObject(id);

        if (!isAdsEnabled())
            return false;

        if(interstitialAd == null)
            return false;

        long adCooldown=300*1000;
        long now = Calendar.getInstance().getTimeInMillis();

        if(!isAdsEnabled() || !interstitialAd.isLoaded() || (now - getLastShown(id)) < adCooldown)
            return false;
        else
        {
            setLastShown(id);
            interstitialAd.show();

            return true;
        }
    }

    @CheckResult
    @Override
    public long getLastShown(int id)
    {
        if(getType(id) == AdType.INTERSTITIAL)
            return mLastShown;
        else
            return super.getLastShown(id);
    }

    @Override
    public void setLastShown(int id)
    {
        if(getType(id) == AdType.INTERSTITIAL)
            mLastShown = Calendar.getInstance().getTimeInMillis();
        else
            super.setLastShown(id);
    }


    @CheckResult
    @Override
    public int getBannerHeight(int id)
    {
        return isAdsEnabled() ? mAdSize.getHeightInPixels(getContext()) : 0;
    }

    @Override
    public void muteAllAds()
    {
        super.muteAllAds();

        MobileAds.setAppMuted(true);
    }

    @Override
    public void unmuteAllAds()
    {
        super.unmuteAllAds();

        MobileAds.setAppMuted(false);
    }
}
