package me.tiju.aes;


interface FragmentChangeListener
{
    void goToAbout();
    void goToSbox();
    void goToPlaintextEdit();
    void goToSettings();
    void goBack();
    void hideKeyboard();
}