package me.tiju.aes;

public interface DismissListener
{
    void onDismiss();
}