package me.tiju.aes;


import androidx.fragment.app.Fragment;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class AboutFragment extends Fragment
{
    public AboutFragment()
    {
        // Required empty public constructor
    }


    @NonNull static AboutFragment newInstance()
    {
        return new AboutFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        setHasOptionsMenu(true);

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_about, container, false);

        TextView appVersion = v.findViewById(R.id.appVersion);

        appVersion.setText(BuildConfig.VERSION_NAME);

        ((TextView)v.findViewById(R.id.attributionView)).setMovementMethod(LinkMovementMethod.getInstance());
        ((TextView)v.findViewById(R.id.donateView)).setMovementMethod(LinkMovementMethod.getInstance());

        return v;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);

        menu.findItem(R.id.menu_about).setVisible(false);
    }
}
