package me.tiju.aes;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.SparseArray;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import androidx.annotation.CheckResult;
import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public abstract class AdManager implements LifecycleListener
{
    private static boolean mAdsEnabled = true;
    final private SparseArray<AdRecord> mAds;
    final List<Integer> mAdIds;
    private static int mLastAd = 1;
    private final WeakReference<Context> mContextRef;

    @Retention(RetentionPolicy.SOURCE)
    @Target({ElementType.TYPE_USE})
    @IntDef({AdType.INVALID, AdType.BANNER, AdType.INTERSTITIAL})
    @interface AdType {int INVALID=-1, BANNER=0, INTERSTITIAL=1; }

    static final int INVALID_ID = -1;
    private boolean mHasConsent=true;

    protected static class AdRecord
    {
        @AdType int type;
        Object object;
        long lastShown;
    }

    AdManager(@NonNull Context context)
    {
        mContextRef = new WeakReference<>(context);
        mAds = new SparseArray<>();
        mAdIds = new ArrayList<>();
    }

    @CheckResult
    public int getBannerHeight(int id)
    {
        return 0;
    }

    @Nullable
    public Context getContext()
    {
        return mContextRef.get();
    }

    public int createAd(int type)
    {
        if (!isAdsEnabled())
            return INVALID_ID;

        AdRecord adRecord = new AdRecord();
        int id;

        synchronized (AdManager.class)
        {
            id = mLastAd++;
        }

        adRecord.type = type;
        adRecord.object = null;

        mAds.append(id, adRecord);
        mAdIds.add(id);

        return id;
    }

    public void destroyAd(int id)
    {
        if (id != INVALID_ID)
        {
            mAds.delete(id);
            mAdIds.remove((Integer)id);
        }
    }

    public void onDestroy(Context context)
    {

    }

    @CheckResult
    public boolean showAd(int id)
    {
        return false;
    }

    public void loadAd(int id)
    {

    }

    @CheckResult @AdType
    int getType(int id)
    {
        if (id == INVALID_ID)
            return AdType.INVALID;

        AdRecord adRecord = mAds.get(id);

        return (adRecord == null) ? AdType.INVALID : adRecord.type;
    }

    @CheckResult @Nullable
    Object getAdObject(int id)
    {
        if (id == INVALID_ID)
            return null;

        AdRecord adRecord = mAds.get(id);

        return (adRecord == null) ? null : adRecord.object;
    }

    void setAdObject(int id, Object object)
    {
        if (id != INVALID_ID)
            mAds.get(id).object = object;
    }

    public long getLastShown(int id)
    {
        if (id == INVALID_ID)
            return -1;

        AdRecord adRecord = mAds.get(id);

        return (adRecord == null) ? -1 : adRecord.lastShown;
    }

    public void setLastShown(int id)
    {
        if (id == INVALID_ID)
            return;

        AdRecord adRecord = mAds.get(id);

        if (adRecord != null)
            adRecord.lastShown = Calendar.getInstance().getTimeInMillis();
    }

    public void muteAd(int id)
    {

    }

    public void unmuteAd(int id)
    {

    }

    public void muteAllAds()
    {

    }

    public void unmuteAllAds()
    {

    }

    public static void enableAds()
    {
        mAdsEnabled = true;
    }

    public static void disableAds()
    {
        mAdsEnabled = false;
    }

    @CheckResult
    static boolean isAdsEnabled()
    {
        return mAdsEnabled;
    }

    public void setConsent(boolean grantConsent)
    {
        if(grantConsent != mHasConsent)
        {
            mHasConsent = grantConsent;
            Context context = getContext();

            if(context != null)
            {
                SharedPreferences preferences = PreferenceManager
                        .getDefaultSharedPreferences(context);

                mHasConsent = preferences
                        .getBoolean(context.getString(R.string.pref_ad_consent), false);
            }
        }
    }

    public boolean hasConsent()
    {
        return mHasConsent;
    }
}
